unit UTipos;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UTipos.pas

Objetivo: Declaraciones de tipos y funciones para inicializar los datos

}

interface


type
  TTipoCasilla = (tc0 = 0,
                  tc1,
                  tc2,
                  tc3,
                  tc4,
                  tc5,
                  tc6,
                  tc7,
                  tc8,
                  tc9,
                  tc10,
                  tc11,
                  tc12,
                  tc13,
                  tc14,
                  tc15,
                  tc16,
                  tc17,
                  tc18,
                  tc19,
                  tc20,
                  tc21,
                  tc22,
                  tc23,
                  tc24);

const
  TextoTipoCasilla: Array[TTipoCasilla] of String = ('0',
                                                     '1',
                                                     '2',
                                                     '3',
                                                     '4',
                                                     '5',
                                                     '6',
                                                     '7',
                                                     '8',
                                                     '9',
                                                     '10',
                                                     '11',
                                                     '12',
                                                     '13',
                                                     '14',
                                                     '15',
                                                     '16',
                                                     '17',
                                                     '18',
                                                     '19',
                                                     '20',
                                                     '21',
                                                     '22',
                                                     '23',
                                                     '24'
                                                     );

type
  TCoordPos = record
  private
    fTipoCasilla: TTipoCasilla;
    fcX: Integer;
    ffY: Integer;
  public
    procedure AsignarDe(ACasilla: TCoordPos);
    function EqualTo(ACasilla: TCoordPos): Boolean;
    constructor Create(AcX: Integer; AfY:Integer; ATipoCasilla: TTipoCasilla);
    function ToString: String;
    property Tipo: TTipoCasilla read fTipoCasilla write fTipoCasilla;
    property cX: Integer read fcX write fcX;
    property fY: Integer read ffY write ffY;
  end;
  TArrayCoordPos = Array of TCoordPos;
  TEstructuraNodos = Array of TArrayCoordPos;

function Get3x3Mapa: TEstructuraNodos;
function Get4x4Mapa: TEstructuraNodos;
function Get5x5Mapa: TEstructuraNodos;


implementation

uses System.SysUtils;

  { TCasillaPos }

procedure TCoordPos.AsignarDe(ACasilla: TCoordPos);
begin
  fcX:= ACasilla.cX;
  ffY:= ACasilla.fY;
  fTipoCasilla:= ACasilla.fTipoCasilla;
end;

constructor TCoordPos.Create(AcX, AfY: Integer; ATipoCasilla: TTipoCasilla);
begin
  fcX:= AcX;
  ffY:= AfY;
  fTipoCasilla:= ATipoCasilla;
end;

function TCoordPos.EqualTo(ACasilla: TCoordPos): Boolean;
begin
  Result:= (fcX = ACasilla.cX) and
           (ffY = ACasilla.fY) and
           (fTipoCasilla = ACasilla.fTipoCasilla);
end;

function TCoordPos.ToString: String;
begin
   Result:= '(' + fY.ToString + ' X; ' + cX.ToString + ' Y)';
end;


function Get3x3Mapa: TEstructuraNodos;
const
  n3x3_00:  TCoordPos = (fTipoCasilla:tc6;    fcX:1; ffY:1);
  n3x3_01:  TCoordPos = (fTipoCasilla:tc3;    fcX:1; ffY:2);
  n3x3_02:  TCoordPos = (fTipoCasilla:tc0;    fcX:1; ffY:3);

  n3x3_03:  TCoordPos = (fTipoCasilla:tc7;    fcX:2; ffY:1);
  n3x3_04:  TCoordPos = (fTipoCasilla:tc4;    fcX:2; ffY:2);
  n3x3_05:  TCoordPos = (fTipoCasilla:tc1;    fcX:2; ffY:3);

  n3x3_06:  TCoordPos = (fTipoCasilla:tc8;    fcX:3; ffY:1);
  n3x3_07:  TCoordPos = (fTipoCasilla:tc5;    fcX:3; ffY:2);
  n3x3_08:  TCoordPos = (fTipoCasilla:tc2;    fcX:3; ffY:3);

 begin

  Result:= [
              [n3x3_00,n3x3_01,n3x3_02],     //columna1
              [n3x3_03,n3x3_04,n3x3_05],     //columna2
              [n3x3_06,n3x3_07,n3x3_08]      //columna3
           ];
end;



function Get4x4Mapa: TEstructuraNodos;
const
  n4x4_00:  TCoordPos = (fTipoCasilla:tc12;   fcX:1; ffY:1);
  n4x4_01:  TCoordPos = (fTipoCasilla:tc8;    fcX:1; ffY:2);
  n4x4_02:  TCoordPos = (fTipoCasilla:tc4;    fcX:1; ffY:3);
  n4x4_03:  TCoordPos = (fTipoCasilla:tc0;    fcX:1; ffY:4);

  n4x4_04:  TCoordPos = (fTipoCasilla:tc13;   fcX:2; ffY:1);
  n4x4_05:  TCoordPos = (fTipoCasilla:tc9;    fcX:2; ffY:2);
  n4x4_06:  TCoordPos = (fTipoCasilla:tc5;    fcX:2; ffY:3);
  n4x4_07:  TCoordPos = (fTipoCasilla:tc1;    fcX:2; ffY:4);

  n4x4_08:  TCoordPos = (fTipoCasilla:tc14;   fcX:3; ffY:1);
  n4x4_09:  TCoordPos = (fTipoCasilla:tc10;   fcX:3; ffY:2);
  n4x4_10:  TCoordPos = (fTipoCasilla:tc6;    fcX:3; ffY:3);
  n4x4_11:  TCoordPos = (fTipoCasilla:tc2;    fcX:3; ffY:4);

  n4x4_12:  TCoordPos = (fTipoCasilla:tc15;   fcX:4; ffY:1);
  n4x4_13:  TCoordPos = (fTipoCasilla:tc11;   fcX:4; ffY:2);
  n4x4_14:  TCoordPos = (fTipoCasilla:tc7;    fcX:4; ffY:3);
  n4x4_15:  TCoordPos = (fTipoCasilla:tc3;    fcX:4; ffY:4);
begin
  Result:= [
              [n4x4_00,n4x4_01,n4x4_02,n4x4_03],     //columna1
              [n4x4_04,n4x4_05,n4x4_06,n4x4_07],     //columna2
              [n4x4_08,n4x4_09,n4x4_10,n4x4_11],     //columna3
              [n4x4_12,n4x4_13,n4x4_14,n4x4_15]      //columna4
           ];
end;

function Get5x5Mapa: TEstructuraNodos;
const
  n5x5_00:  TCoordPos = (fTipoCasilla:tc20;   fcX:1; ffY:1);
  n5x5_01:  TCoordPos = (fTipoCasilla:tc15;   fcX:1; ffY:2);
  n5x5_02:  TCoordPos = (fTipoCasilla:tc10;   fcX:1; ffY:3);
  n5x5_03:  TCoordPos = (fTipoCasilla:tc5;    fcX:1; ffY:4);
  n5x5_04:  TCoordPos = (fTipoCasilla:tc0;    fcX:1; ffY:5);

  n5x5_05:  TCoordPos = (fTipoCasilla:tc21;   fcX:2; ffY:1);
  n5x5_06:  TCoordPos = (fTipoCasilla:tc16;   fcX:2; ffY:2);
  n5x5_07:  TCoordPos = (fTipoCasilla:tc11;   fcX:2; ffY:3);
  n5x5_08:  TCoordPos = (fTipoCasilla:tc6;    fcX:2; ffY:4);
  n5x5_09:  TCoordPos = (fTipoCasilla:tc1;    fcX:2; ffY:5);

  n5x5_10:  TCoordPos = (fTipoCasilla:tc22;   fcX:3; ffY:1);
  n5x5_11:  TCoordPos = (fTipoCasilla:tc17;   fcX:3; ffY:2);
  n5x5_12:  TCoordPos = (fTipoCasilla:tc12;   fcX:3; ffY:3);
  n5x5_13:  TCoordPos = (fTipoCasilla:tc7;    fcX:3; ffY:4);
  n5x5_14:  TCoordPos = (fTipoCasilla:tc2;    fcX:3; ffY:5);

  n5x5_15:  TCoordPos = (fTipoCasilla:tc23;   fcX:4; ffY:1);
  n5x5_16:  TCoordPos = (fTipoCasilla:tc18;   fcX:4; ffY:2);
  n5x5_17:  TCoordPos = (fTipoCasilla:tc13;   fcX:4; ffY:3);
  n5x5_18:  TCoordPos = (fTipoCasilla:tc8;    fcX:4; ffY:4);
  n5x5_19:  TCoordPos = (fTipoCasilla:tc3;    fcX:4; ffY:5);

  n5x5_20:  TCoordPos = (fTipoCasilla:tc24;   fcX:5; ffY:1);
  n5x5_21:  TCoordPos = (fTipoCasilla:tc19;   fcX:5; ffY:2);
  n5x5_22:  TCoordPos = (fTipoCasilla:tc14;   fcX:5; ffY:3);
  n5x5_23:  TCoordPos = (fTipoCasilla:tc9;    fcX:5; ffY:4);
  n5x5_24:  TCoordPos = (fTipoCasilla:tc4;    fcX:5; ffY:5);

begin
  Result:= [
              [n5x5_00, n5x5_01, n5x5_02, n5x5_03, n5x5_04], //columna1
              [n5x5_05, n5x5_06, n5x5_07, n5x5_08, n5x5_09], //columna2
              [n5x5_10, n5x5_11, n5x5_12, n5x5_13, n5x5_14], //columna3
              [n5x5_15, n5x5_16, n5x5_17, n5x5_18, n5x5_19], //columna4
              [n5x5_20, n5x5_21, n5x5_22, n5x5_23, n5x5_24]  //columna5
           ];
end;


end.
