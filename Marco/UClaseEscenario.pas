unit UClaseEscenario;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClaseEscenario.pas

Objetivo: La clase TClaseEscenario, la utilizamos como contenedor de la estructura
          del grafo, que en este contexto se representa por el puzzle. En otro contexto
          podrian ser jugadores distintos que resuelven el mismo problema (ver
          el video de la resolucion de rutas de la entrada:
          https://delphibasico.com/2017/08/27/tiempo-para-leer-virginie-mathivet/
          El video mostraba dos jugadores con estrategias distintas para recorrer
          un mapa X, aunque solo se ejecuta uno)

}

interface

uses   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
Generics.Collections, UClasePuzzle;

type
  // Clase contenedor de puzzles

  TClaseEscenario = class(TObject)
  private
    FPuzzles: TList<TPuzzle>;
    FFilas: Integer;
    FColumnas: Integer;
    function GetPuzzle(Index: Integer): TPuzzle;
    procedure SetColumnas(const Value: Integer);
    procedure SetFilas(const Value: Integer);
  protected
  public
    constructor Create(ANumFilas, ANumColumnas: Integer);
    destructor Destroy; override;
    function AddPuzzle(AIGrafo: TPuzzle): Integer;
    function Count: Integer;
    property Columnas: Integer read FColumnas write SetColumnas;
    property Filas: Integer read FFilas write SetFilas;
    procedure DeletePuzzle(AIndex: Integer);
    property Puzzles[Index: Integer]: TPuzzle read GetPuzzle; default;
  end;


implementation


{ TClaseEscenario }

{
****************************** TClaseEscenario ********************************
}

function TClaseEscenario.AddPuzzle(AIGrafo: TPuzzle): Integer;
begin
  Result:= FPuzzles.Add(AIGrafo);
end;

function TClaseEscenario.Count: Integer;
begin
  Result:= FPuzzles.Count;
end;

constructor TClaseEscenario.Create(ANumFilas, ANumColumnas: Integer);
begin
  FColumnas:= ANumColumnas;
  FFilas:= ANumFilas;
  FPuzzles:= TList<TPuzzle>.Create;
end;

procedure TClaseEscenario.DeletePuzzle(AIndex: Integer);
begin
  FPuzzles.Delete(AIndex);
end;

destructor TClaseEscenario.Destroy;
var
  i: Integer;
begin
  for I := 0 to FPuzzles.Count-1 do FPuzzles[i].Free;
  FPuzzles.Clear;
  FPuzzles.Free;
  inherited;
end;

function TClaseEscenario.GetPuzzle(Index: Integer): TPuzzle;
begin
  Result:= FPuzzles[Index];
end;

procedure TClaseEscenario.SetColumnas(const Value: Integer);
begin
  FColumnas := Value;
end;

procedure TClaseEscenario.SetFilas(const Value: Integer);
begin
  FFilas := Value;
end;


end.
