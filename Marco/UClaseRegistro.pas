unit UClaseRegistro;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClaseRegistro.pas

Objetivo: Las clases TModuleInfo y TModuleInfoManager, permiten gestionar el
          acceso a una clase mediante su nombre. Ambas actuan de forma conjunta
          y podriais encontrar mas informaci�n en la entrada que abordaba la
          modularidad de la aplicacion:

          https://delphibasico.com/2007/10/19/un-enfoque-modular-para-nuestra-aplicacion-fuentes/
          https://delphibasico.files.wordpress.com/2007/10/framework.pdf

          Esta entrada y el pdf con el contenido de los art�culos publicados en
          Sintesis, mostraban esa interacci�n y desacoplaban la aplicaci�n respecto
          de los modulos.

          Como siempre, intentamos reaprovechar aquello que puede ser reutilizado y
          en este caso se ha adaptado para que podamos cargar las distintas
          implementaciones del algoritmo.

}


interface

uses   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, Generics.Collections,
  UClaseGrafo, UClasePuzzle;


type
  TfrmCustomAlgoritmoClass = class of TAlgoritmoThread;

// Contiene informaci�n acerca del modulo que contiene un descendiente de
// la clase TfrmCustomAlgoritmoClass. Permite ligar el nombre de la clase y la clase

  TModuleInfo = class(TObject)
  private
    FImageIndex: Integer;
    FModule: TAlgoritmoThread;
    FModuleClass: TfrmCustomAlgoritmoClass;
    FName: string;
    FCaption: string;
  protected
  public
    constructor Create(const AName: string; AModuleClass: TfrmCustomAlgoritmoClass;const ACaption: String = ''; AImageIndex: Integer = -1);
    destructor Destroy; override;
    property ImageIndex: Integer read FImageIndex;
    property Module: TAlgoritmoThread read FModule;
    property Name: string read FName;
    property Caption: string read FCaption;
  end;

   //Clase experta en la gestion de los m�dulos

  TModuleInfoManager = class(TObject)
  private
    FModuleList: TObjectList<TModuleInfo>;
    function GetCount: Integer;
    function GetItem(Index: Integer): TModuleInfo;
  public
    constructor Create;
    destructor Destroy; override;
    function GetModuleInfoByName(const AName: string): TModuleInfo;
    function GetModuleClassByName(const AName: string): TfrmCustomAlgoritmoClass;
    procedure RegisterModule(const AName: string; AModuleClass: TfrmCustomAlgoritmoClass; const ACaption: String = ''; AImageIndex: Integer = -1);
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TModuleInfo read GetItem; default;
  end;

// Devuelve una instancia global de la clase experta TModuleInfoManager
function ModuleInfoManager: TModuleInfoManager;

implementation



{ TModuleInfo }



{
********************************* TModuleInfo **********************************
}
constructor TModuleInfo.Create(const AName: string; AModuleClass: TfrmCustomAlgoritmoClass;const ACaption: String = ''; AImageIndex: Integer = -1);
begin
  FName := AName;
  FModuleClass := AModuleClass;
  FImageIndex := AImageIndex;
  FCaption:= ACaption;
end;

destructor TModuleInfo.Destroy;
begin
  //
  inherited Destroy;
end;



{ TModuleInfoManager }

{
****************************** TModuleInfoManager ******************************
}

constructor TModuleInfoManager.Create;
begin
  FModuleList := TObjectList<TModuleInfo>.Create(True);
end;

destructor TModuleInfoManager.Destroy;
begin
  FModuleList.Clear;
  FModuleList.DisposeOf;
  inherited;
end;

function TModuleInfoManager.GetCount: Integer;
begin
  Result := FModuleList.Count;
end;

function TModuleInfoManager.GetItem(Index: Integer): TModuleInfo;
begin
  Result := FModuleList[Index];
end;

function TModuleInfoManager.GetModuleClassByName(const AName: string): TfrmCustomAlgoritmoClass;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if(CompareText(Items[I].Name, AName) = 0) then
    begin
      Result := Items[I].FModuleClass;
      break;
    end;
end;

function TModuleInfoManager.GetModuleInfoByName(const AName: string):
        TModuleInfo;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if(CompareText(Items[I].Name, AName) = 0) then
    begin
      Result := Items[I];
      break;
    end;
end;

procedure TModuleInfoManager.RegisterModule(const AName: string; AModuleClass:
        TfrmCustomAlgoritmoClass; const ACaption: String = ''; AImageIndex: Integer = -1);
var
  AModuleInfo: TModuleInfo;
begin
  //Aqui se podria revisar la seguridad antes de registrar el modulo
  AModuleInfo := GetModuleInfoByName(AName);
  //Es lanzada una excepcion si el modulo ya existe con el mismo nombre
  if(AModuleInfo <> nil) then
    raise Exception.CreateFmt('El m�dulo con nombre "%s" ya existe', [AName]);
  // Creamos una categoria si no existe todav�a
  // Creamos la instancia de informaci�n y la a�adimos a la lista
  AModuleInfo := TModuleInfo.Create(AName, AModuleClass, ACaption, AImageIndex);
  FModuleList.Add(AModuleInfo);
end;


var
  FModuleInfoManager: TModuleInfoManager = nil;

function ModuleInfoManager: TModuleInfoManager;
begin
  if FModuleInfoManager = nil then
    FModuleInfoManager := TModuleInfoManager.Create;
  Result := FModuleInfoManager;
end;

initialization

finalization
  FModuleInfoManager.Free;
  FModuleInfoManager := nil;

end.
