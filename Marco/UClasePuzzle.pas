unit UClasePuzzle;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClasePuzzle.pas

Objetivo: La clase TPuzzle descendiente de TGrafo y concreta sus propiedades
          y m�todos para resolver el problema. Representa al problema 8Puzzle
          En las cabeceras de metodo de la implementaci�n se han a�adidos
          detalles adicionales de los metodos que expone la clase.

}

interface

Uses UClaseGrafo, FMX.Dialogs, Generics.Collections;

type
  TPuzzle = class(TGrafo)
  private
    FPuzzlePrevio: TPuzzle;
    FCoste: Single;
    FCantidadDesplazadas: Integer;
    FDistanciaManhatan: Single;
    function GetDesplazadas: Integer;
    function GetDistanciaManhatan: Single;
    function GetCoste: Single;
    function GetPrioridad: Single;
    function GetPuzzlePrevio: TPuzzle;
    procedure SetCoste(const Value: Single);
    procedure SetPuzzlePrevio(const Value: TPuzzle);
  protected
     function Mover(AIDXSource: Integer; DestZero: Integer): Integer; overload;
  public
    function Clonar: TPuzzle;
    procedure Copiar(APuzzle: TArrayInteger);
    function EvaluarExitoMovimiento: Boolean;
    class function ExtraerPrioridadMinima(AMovimientos: Array of TPuzzle): TPuzzle;
    function Equals(const Value: TPuzzle): Boolean;
    function GetIDXCero: Integer;
    function GetSolucion: TArrayInteger; overload;
    function GetSolucionAsString: String;
    procedure Mezclar(var ANumMezclas: Integer);
    function Mover(AIDXSource: Integer): Integer; overload;
    procedure Reset;
    function ToString: String;
    function CalcularDistanciaManhatan: Single;
    property Coste: Single read GetCoste write SetCoste;
    property Desplazadas: Integer read GetDesplazadas;
    property DistanciaManhatan: Single read GetDistanciaManhatan;
    property Prioridad: Single read GetPrioridad;
    property PuzzlePrevio: TPuzzle read GetPuzzlePrevio write SetPuzzlePrevio;
  end;

  TArrayPuzzle = Array of TPuzzle;


implementation

uses System.Classes, System.SysUtils, System.UITypes, System.Math,
System.Generics.Defaults, UTipos;


{ TPuzzle }






{
  Metodo: CalcularDistanciaManhatan

  Descripcion: C�lculo de la distancia manhatan.
               Se actualiza la cantidad de casillas desplazadas del objetivo.


}
function TPuzzle.CalcularDistanciaManhatan: Single;
var
  ni: Integer;
  n: TNodo;
  nSol: TNodo;
  iSol: TArrayInteger;
  i,j: Integer;
  tc: TTipoCasilla;
begin                   // 012345678  idx
  iSol:= GetSolucion;   //'630741852' valor

  FDistanciaManhatan:= 0;
  FCantidadDesplazadas:= 0;

  for ni:= 0 to Count-1 do
  begin
    n:= Nodos[ni];
    tc:= n.TipoCasilla;

    if tc <> tc0 then
    begin
      //buscamos en la solucion para ese tipo de casilla
      for j:= Low(iSol) to High(iSol) do
      begin
        if iSol[j] = Integer(tc) then
        begin
          nSol:= GetNodo(j);
          Break;
        end;
      end;
      //calculamos la distancia manhatan entre las casillas
      n.DistanciaADestino:= Abs(n.fY - nSol.fY) + Abs(n.cX - nSol.cX);
      //cada vez que calculamos la distancia manhatan
      //actualizamos las casillas desplazadas del destino
      if n.DistanciaADestino > 0 then Inc(FCantidadDesplazadas);

      //acumulamos el resultado para poder saber cual es la mejor opci�n
      //que debe coincidir con la distancia manhatan mas corta
      FDistanciaManhatan := FDistanciaManhatan + n.DistanciaADestino;

    end;
  end;
  Result:= FDistanciaManhatan;
end;


{
  Metodo: Clonar

  Descripcion: El m�todo crea una nueva instancia de TPuzzle y copia los
               valores de tipo de cada casilla, as� como el coste.
               No tiene en cuenta el puzzle previo, que es propio del algoritmo.
               Tampoco tiene en cuenta los recalculos ni la asignacion del
               selector.

}
function TPuzzle.Clonar: TPuzzle;
var
  nIDX: Integer;
begin
  Result:= TPuzzle.Create(GetOutForm, NumFilas, NumColumnas);
  Result.Coste:= Coste;
  for nIDX in ListaNodos do
    Result.GetNodo(nIDX).TipoCasilla:= self.GetNodo(nIDX).TipoCasilla;
end;


{
  Metodo: Copiar

  Descripcion:  Similar a Clonar, pero sin la creaci�n de una nueva instancia.
                Actualizamos la actual respecto a otra.
                Se ha a�adido el calculo de la distanciamanhatan a efectos
                ilustrativos.
                La usamos desde el interfaz principal para visualizar la lista
                de pasos que obtenemos y que se despliega en el listbox.

}
procedure TPuzzle.Copiar(APuzzle: TArrayInteger);
var
  i: Integer;
begin
  for i in ListaNodos do
     Nodos[i].TipoCasilla:= TTipoCasilla(APuzzle[i]);

  CalcularDistanciaManhatan;
end;


{
  Metodo: Equals

  Descripcion: Comparamos dos Puzzles a trav�s de su disposici�n respecto
               a las casillas.
               El metodo ToString genera una cadena de texto donde se listan
               las posiciones en el orden en el que se encuentran en la lista
               de nodos.
               Ej:
                 [6,3,0,7,4,1,8,5,2]
               De esa forma, comparamos exclusivamente por la tipologia de
               las casillas.

}
function TPuzzle.Equals(const Value: TPuzzle): Boolean;
var
  s1, s2: String;
begin
  s1:= self.ToString;
  s2:= Value.ToString;
  Result:= (s1 = s2);
end;


{
  Metodo: EvaluarExitoMovimiento

  Descripcion: El m�todo compara la instancia actual con el puzzle que
               representa el objetivo o soluci�n.
               Por defecto, el mapa que es cargado, se toma como objetivo.

               Nuestro objetivo es:

                      0  1  2
                      3  4  5
                      6  7  8

               Este punto es algo que se puede modificar tan solo modificando
               estos mapas cargados al inicio en la unidad UTipos.pas

}
function TPuzzle.EvaluarExitoMovimiento: Boolean;
var
  i: Integer;
  s: String;
begin
  s:= '';
  for i:= 0 to Count-1 do
    s:= s + Integer(Nodos[i].TipoCasilla).ToString;
  Exit(GetSolucionAsString = s);
end;


{
  Metodo: ExtraerPrioridadMinima

  Descripcion:


}
class function TPuzzle.ExtraerPrioridadMinima(
  AMovimientos: array of TPuzzle): TPuzzle;
var
  i: Integer;
begin
  if Length(AMovimientos) > 0 then
  begin
    Result:= AMovimientos[Low(AMovimientos)];
    for i := Low(AMovimientos) to High(AMovimientos)-1 do
      if AMovimientos[i+1].Prioridad <= Result.Prioridad then
      begin
           Result:= AMovimientos[i+1];
      end;
  end
  else Result:= nil;
end;


{
  Metodo: GetCoste

  Descripcion: Cada vez que invocamos el metodo Mover( ), el coste se incrementa
               en 1 unidad, por lo tanto, el coste representa la distancia
               sobre el punto de inicio en funcion de los movimientos hechos.

}
function TPuzzle.GetCoste: Single;
begin
  Result:= FCoste;
end;


{
  Metodo: GetDesplazadas

  Descripcion: El metodo devuelve la suma total de casillas desplazadas
               respecto de su posici�n de origen respectiva

               Situacion actual   Situacion objetivo     Matriz Desplazadas
                  2  1  6              0  1  2               1  0  1
                  3  4  0              3  4  5               0  0  0   =  5
                  7  8  5              6  7  8               1  1  1

               No contamos la casilla cero

               Este valor se obtiene durante la ejecucion del m�todo
               CalcularDistanciaManhatan. Por ello es una propiedad de solo lectura.
}
function TPuzzle.GetDesplazadas: Integer;
begin
  Result:= FCantidadDesplazadas;
end;


{
  Metodo: GetDistanciaManhatan

  Descripcion: M�todo de lectura de la propiedad DistanciaManhatan.


}
function TPuzzle.GetDistanciaManhatan: Single;
begin
  Result:= FDistanciaManhatan;
end;


{
  Metodo: GetIDXCero

  Descripcion: El m�todo obtiene el valor del indice para la instancia actual,
               para la casilla con tipo tc0, que representa el cero.


}
function TPuzzle.GetIDXCero: Integer;
var
 i: Integer;
begin
 Result:= -1;
 for i:= 0 to Count-1 do
    if Nodos[i].TipoCasilla = tc0 then
      Exit(Nodos[i].IDX);
end;



{
  Metodo: GetPrioridad

  Descripcion: El m�todo fija el valor que ordena la lista de prioridad, en
               el uso de algoritmos que necesiten calcular la distancia al
               punto de soluci�n. Un buen ejercicio es adoptar diversos criterios
               para experimentar en que medida somos capaces de extraer mejores
               decisiones a la hora de efectuar sucesivos movimientos.
               Yo opte en las pruebas en considerar por ejemplo la prioridad como
               la suma de las distancias al punto de origen mas la suma de las
               distancias manhatan al punto de exito y la cantidad de casillas
               desplazadas.

}
function TPuzzle.GetPrioridad: Single;
begin
  Result:= {FCantidadDesplazadas +} FDistanciaManhatan + FCoste;
end;


{
  Metodo: GetPuzzlePrevio

  Descripcion: El m�todo representa el estado del puzzle antes de efectuar un
               movimiento. Este m�todo de lectura permitir� resolver una estrategia
               de backtracking, de vuelta atr�s, para localizar la rama del arbol
               de grafos que ha tenido exito, entre todas las ramas recorridas.

}
function TPuzzle.GetPuzzlePrevio: TPuzzle;
begin
  Result:= FPuzzlePrevio;
end;


{
  Metodo: GetSolucionAsString

  Descripcion: Devuelve la solucion en formato cadena.

}
function TPuzzle.GetSolucionAsString: String;
var
  i: Integer;
  Sol: TArrayInteger;
begin
  Result:= '';
  Sol:= GetSolucion;
  for i := Low(Sol) to High(Sol) do
    Result:= Result + TextoTipoCasilla[TTipoCasilla(Sol[i])];
end;



{
  Metodo: GetSolucion

  Descripcion: Devuelve la solucion como un Array de Enteros.

               Nota importante: Ademas de obtener la solucion en
               formato de texto parecia conveniente obtenerla tambien con el
               tipo del array de enteros,  que te permite recorrer los nodos
               mas facilmente.

}
function TPuzzle.GetSolucion: TArrayInteger;
var
  i: Integer;
  n: TNodo;
  fEN: TEstructuraNodos;
  ifcX, iffY: Integer;
begin
  Result:= [];

  fEN:= TGrafo.LoadMapa(NumFilas, NumColumnas);

  SetLength(Result, NumFilas*NumColumnas);

  for i:= 0 to Count-1 do
  begin
    n:= Nodos[i];
    ifcX:= GetCoord(n.IDX).cX;
    iffY:= GetCoord(n.IDX).fY;
    Result[i]:= Integer(fEN[ifcX-1][iffY-1].Tipo);
  end;
end;


{
  Metodo:

  Descripcion:


}
procedure TPuzzle.Mezclar(var ANumMezclas: Integer);
var
 I: Integer;
 nIDX: Integer;
 nIDXCero: Integer;
 tmp: TTipoCasilla;
begin
  for i:= 0 to Count-1 do
    if Nodos[i].TipoCasilla = tc0 then
    begin
      nIDXCero:= Nodos[i].IDX;
      Break;
    end;

  while  ANumMezclas > 0 do
  begin
     repeat
       nIDX:= nIDXCero;
       while (nIDXCero = nIDX) do  nIDX:= Random(GetDimension);
     until (
               Nodos[nIDX].ALaIzquierdaDe(Nodos[nIDXCero]) or
               Nodos[nIDX].ALaDerechaDe(Nodos[nIDXCero]) or
               Nodos[nIDX].AbajoDe(Nodos[nIDXCero]) or
               Nodos[nIDX].ArribaDe(Nodos[nIDXCero])
           );

     tmp:= Nodos[nIDX].TipoCasilla;
     Nodos[nIDX].TipoCasilla:= tc0;
     Nodos[nIDXCero].TipoCasilla:= tmp;
     AsignaSelector(nIDX);
     nIDXCero:= nIDX;
     Dec( ANumMezclas);
     if GetOutForm <> Nil then GetOutForm.Update(Self);
  end;
  FCoste:= 0;
end;


{
  Metodo: Mover

  Descripcion: El m�todo permite mover en bloque cuando la distancia entre
               la casilla deseada y la casilla zero sea mayor a 1, siempre
               y cuando ambas se encuentren en la misma fila o columna.
               Internamente, es la ejecucion del M�todo Mover( ) para distancia
               de 1 repetido multiples veces.

               Ej:  Situacion inicial  Objetivo          Equivalencia
                   ---------------------------------------------------------
                    X  X  X            X  X  X         X  X  X      X  X  X
                    X  X  0    =>      0  X  X    =    0  X  X   +  X  0  X
                    X  X  X            X  X  X         X  X  X      X  X  X


}
function TPuzzle.Mover(AIDXSource, DestZero: Integer): Integer;
begin
  if (DestZero <> AIDXSource) and (
               Nodos[DestZero].ALaIzquierdaDe(Nodos[AIDXSource]) or
               Nodos[DestZero].ALaDerechaDe(Nodos[AIDXSource]) or
               Nodos[DestZero].AbajoDe(Nodos[AIDXSource]) or
               Nodos[DestZero].ArribaDe(Nodos[AIDXSource])) then
     Result:= Mover(AIDXSource)
  else
  if (Nodos[AIDXSource].cX = Nodos[DestZero].cX) or
     (Nodos[AIDXSource].fY = Nodos[DestZero].fY) then
  begin
    // la columna x es la misma, cambia la fila y
    if (Nodos[AIDXSource].cX = Nodos[DestZero].cX) then
    begin
      if (Nodos[AIDXSource].fY > Nodos[DestZero].fY)  then
           Mover(GetIndice(Nodos[DestZero].cX, Nodos[DestZero].fY+1))
      else
       Mover(GetIndice(Nodos[DestZero].cX, Nodos[DestZero].fY-1));
    end
    else if (Nodos[AIDXSource].fY = Nodos[DestZero].fY) then
          begin
            if (Nodos[AIDXSource].cX > Nodos[DestZero].cX)  then
                 Mover(GetIndice(Nodos[DestZero].cX+1, Nodos[DestZero].fY), DestZero)
            else Mover(GetIndice(Nodos[DestZero].cX-1, Nodos[DestZero].fY), DestZero)
          end;
    Mover(AIDXSource);
  end
  else Result:= -1;
end;


{
  Metodo: Mover

  Descripcion: El m�todo permite mover en bloque cuando la distancia entre
               la casilla deseada y la casilla zero sea igual a 1, siempre
               y cuando ambas se encuentren en la misma fila o columna.

               Ej:  Situacion inicial  Objetivo
                   ------------------------------
                    X  X  X            X  X  X
                    X  X  0    =>      X  0  X
                    X  X  X            X  X  X

}
function TPuzzle.Mover(AIDXSource: Integer): Integer;
var
 i: Integer;
 nIDX: Integer;
 nIDXCero: Integer;
 tmp: TTipoCasilla;
begin
  nIDX:= AIDXSource;

  for i:= 0 to Count-1 do
    if Nodos[i].TipoCasilla = tc0 then
    begin
      nIDXCero:= Nodos[i].IDX;
      Break;
    end;

  if (nIDXCero <> nIDX) and (
               Nodos[nIDXCero].ALaIzquierdaDe(Nodos[nIDX]) or
               Nodos[nIDXCero].ALaDerechaDe(Nodos[nIDX]) or
               Nodos[nIDXCero].AbajoDe(Nodos[nIDX]) or
               Nodos[nIDXCero].ArribaDe(Nodos[nIDX])) then
  begin
     tmp:= Nodos[nIDX].TipoCasilla;
     Nodos[nIDX].TipoCasilla:= tc0;
     Nodos[nIDXCero].TipoCasilla:= tmp;
     //el actual toma el valor del anterior + 1
     AsignaSelector(nIDX);
     Result:= nIDXCero;
     FCoste:= FCoste + 1;
     CalcularDistanciaManhatan;
  end
  else Mover(nIDX, nIDXCero);
end;


{
  Metodo: Reset

  Descripcion: El metodo inicializa el puzzle a la posicion de exito.


}
procedure TPuzzle.Reset;
var
  i: Integer;
  n: TNodo;
  fEN: TEstructuraNodos;
  ifcX, iffY: Integer;
begin
  fEN:= TGrafo.LoadMapa(NumFilas, NumColumnas);

  for i:= 0 to Count-1 do
  begin
    n:= Nodos[i];
    ifcX:= GetCoord(n.IDX).cX;
    iffY:= GetCoord(n.IDX).fY;
    n.TipoCasilla:= fEN[ifcX-1][iffY-1].Tipo;
  end;
  FCoste:= 0;
  FDistanciaManhatan:= 0;
end;


{
  Metodo: SetCoste


  Descripcion: Metodo escritura de la propiedad Coste

}
procedure TPuzzle.SetCoste(const Value: Single);
begin
  FCoste:= Value;
end;



{
  Metodo: SetPuzzlePrevio

  Descripcion: M�todo de escritura de la propiedad PuzzlePrevio

}
procedure TPuzzle.SetPuzzlePrevio(const Value: TPuzzle);
begin
   FPuzzlePrevio:= Value;
end;


{
  Metodo: ToString

  Descripcion: Identificador textual del estado del puzzle, representado por
               la tipologia de las casillas


}
function TPuzzle.ToString: String;
var
 i: Integer;
begin
  Result:= '[';
  for i:= 0 to Count-1 do
  begin
    if i = 0 then
      Result:= Result + Integer(Nodos[i].TipoCasilla).ToString
    else Result:= Result + ',' + Integer(Nodos[i].TipoCasilla).ToString;
  end;
  Result:= Result + ']';
end;



initialization

 Randomize;


end.
