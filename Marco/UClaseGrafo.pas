unit UClaseGrafo;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClaseGrafo.pas

Objetivo: El modulo contiene un conjunto de clases que representan los conceptos
          de grafo y nodo y el algoritmo de resolucio�n de un problema cualquiera
          factible de ser representado mediante esta estructura.

          El nodo, en el problema del puzzle representa una casilla del tablero
          por lo que maneja informaci�n asociada a ese contexto y otra informaci�n
          asociada a la estructura que la gestiona, como el indice que le identifica
          dentro de la misma.
          Algunas propiedades se han personalizado en funcion del problema:
          * TipoCasilla en un problema de rutas puede identificar al tipificaci�n
            del coste implicito a la naturaleza del nodo. Y en el caso del puzzle,
            tipo de casilla representa un identificador asimilado a un numerico,
            para diferenciar el estado del tablero respecto a la solucion.
          * Coste en el caso de puzzle, representa los movimientos hechos desde
            un estado A hasta un estado B que habitualmente resuelve o finaliza,
            de forma que puede ser usado para medir la distancia entre ambas. Este
            coste combinado con otras funciones de medici�n establecen prioridades
            para seleccionar un movimiento entre una lista de posibles movimientos.
          * Accesible no es utilizado en el problema del puzzle pero si en el caso
            de problemas de rutas, indicando si el movimiento es valido o no. Por
            ejemplo, en el libro de V. Mathivet, el mapa contiene algunas casillas
            por las que no se podia pasar (representadas en el arbol o el agua). En
            ese caso, el valor de esta funci�n puede ir intimamente ligado al tipo
            de casilla. Aqui siempre toma valor True, pero es declarada como virtual
            para que pueda ser sobrescrita.
          * cX y fY representan la coordenada en el tablero (columnas y filas). Cada
            nodo identifica una coordenada.
          * IDX es el indice �nico que identifica al nodo en el grafo.
          * DistanciaADestino, en el problema del Puzzle, representa el calculo de
            la distancia Manhatan, que mide la distancia entre ese nodo y su posicion
            que ocupa en la posicion que resuelve. Eso permite al grafo, obtener
            una suma de todas las distancias (exceptuando la casilla vacia) para
            obtener una medici�n de la distancia al objetivo.
          * Funciones de posicionamiento: devuelven si es verdadero o falso que
            un nodo se posiciona respecto a otro, si est� a la izquierda, a la
            derecha, arriba u abajo.

          La clase TGrafo representa al estado del tablero en un momento fijo. Desde
          el punto de vista de la estructura, habitualmente se representa como una
          matriz multidimensional en funcion de su naturaleza (2D,3D...) pero en
          este caso, he optado por elegir una estructura lineal, como es la generica
          TObjectList<TNodo>. Esta estructura lineal de una dimesion se convierte
          a la matriz, en funcion del numero de columnas y filas.

          Equivalencias

          Lista de Nodos (indices)   0 - 1 - 2  - 3 - 4 - 5 - 6 - 7 - 8

          Matiz 3x3                  2   5   8
                                     1   4   7
                                     0   3   6

          El Grafo tiene acceso a la interfaz, que es parametrizada durante el
          evento de creaci�n de la instancia, en el constructor.
          Sirve de abstraccion para resolver otros problemas, de forma que pueda
          ser reutilizada en un momento posterior.
          El Grafo expone distintos metodos que permiten la gestion de los nodos.

          Para optimizar la gestion de memoria, he optado en algunos casos por
          sustituir en los retornos la instancia de una clase por su identificador.
          Como ejemplo se puede ver la funcion:
          function GetNodosAdyadecentes(ANodoIDX: Integer): TIntegerIDXArray;
          Inicialmente devolv�a una lista o array de nodos. En su lugar, la sustitucion
          devuelve una lista o array de enteros, que pueden posteriormente, ser
          usados igualmente para el acceso a la instancia, mediante la propiedad
          Nodos[].
          Para visualmente remarcar esto, aquellas funciones que devuelven un
          indice, se ha marcado el retorno con al sufijo IDX, de esa forma me
          permitia recordarlo mas facilmente.

          Nota: He usado como ascendente de las clases, TInterfacedObject para
                proponer posteriormente extraer una interfaz IGrafo.

          La clase TAlgoritmoThread est� intimamente ligada a TGrafo y representa
          el metodo de resolucion del problema, sin implementaci�n, como abstraccion.
          Se ejecuta en el contexto de un hilo adicional.

          El metodo Ejecutar crea esta relacion e inicia la invocacion:

          procedure TGrafo.Ejecutar(AClass: TClaseAlgoritmo; ACreateSuspended: Boolean);
          begin
            with AClass.Create(ACreateSuspended,Self, FOutForm) do
            begin
             OnTerminate:= OnTerminateThread;
             Start;
            end;
          end;

          Importante: Al crear la resolucion dentro del contexto de un hilo adicional
          nos permitir� evaluar respecto al tablero distintas estrategias que pueden
          enfrentarse o colaborar. Podemos hacer que compitan entre si para ver
          cual de ellas encuentra antes la solucion (era el caso que se podia ver
          en el video de las rutas.

          El concepto de Selector, est� intimamente ligado al tipo de problema.
          En el caso del puzzle, el nodo selector siempre es el que contiene el
          vacio.


}

interface

uses System.Classes, FMX.Graphics, Generics.Collections, UTipos;

type
  TGrafo = class;

  TNodo = class(TInterfacedObject)
  private
    FDistanciaADestino: Double;
    FNodoPrecedente: TNodo;
    FIDX: Integer;
    Grafo: TGrafo;
    FTipoCasilla: TTipoCasilla;
    function GetCoste: Single; virtual;
    function GetIDX: Integer;
    function GetTipoCasilla: TTipoCasilla;
    procedure SetDistanciaADestino(const Value: Double);
    procedure SetIDX(const Value: Integer);
    procedure SetNodoPrecedente(const Value: TNodo);
    procedure SetTipoCasilla(ATipoCasilla: TTipoCasilla);
    function GetInternalcX: Integer;
    function GetInternalfY: Integer;
  public
    constructor Create(AGrafo: TGrafo); virtual;
    destructor Destroy; override;
    //funciones de posicionamiento relativo
    function ALaIzquierdaDe(ANodo: TNodo): Boolean;
    function ALaDerechaDe(ANodo: TNodo): Boolean;
    function AbajoDe(ANodo: TNodo): Boolean;
    function ArribaDe(ANodo: TNodo): Boolean;
    //**************************************
    function Accesible: Boolean; virtual;
    function Clonar: TNodo; virtual;
    function ToString: String; override;
    function ToCoordPos: TCoordPos;
    property cX: Integer read GetInternalcX;
    property fY: Integer read GetInternalfY;
    property Coste: Single read GetCoste;
    property DistanciaADestino: Double read FDistanciaADestino write SetDistanciaADestino;
    property IDX: Integer read GetIDX write SetIDX;  //indice interno del nodo
    property NodoPrecedente: TNodo read FNodoPrecedente write SetNodoPrecedente;
    property TipoCasilla: TTipoCasilla read GetTipoCasilla write SetTipoCasilla;
  end;

  TArrayNodo = Array of TNodo;
  TArrayInteger = Array of Integer;
  TArrayOfArrayInteger = Array of TArrayInteger;

  IInterfaz = Interface
   ['{B395CCCF-EB3E-46A8-AEBC-741BE67C2F2A}']
    procedure Update(const AGrafo: TGrafo);
    procedure FinalizaAlgoritmo(const AGrafo: TGrafo; AFinishDateTime: TDateTime; APasosResolucion: TArrayOfArrayInteger);
  end;

  TAlgoritmoThread = class(TThread)
  private
  protected
    Interfaz: IInterfaz;
    Grafo: TGrafo;
    procedure Run; virtual; abstract;
  protected
    procedure DoBorrarNodos;
    procedure DoActualizaNodoSelectorEnInterfaz; virtual;
    procedure DoFinaliza; virtual;
  public
    constructor Create(CreateSuspended:Boolean; AGrafo: TGrafo; AInterfaz: IInterfaz); overload;
    procedure Execute(); override;
    procedure Resolver;
  end;

  TClaseAlgoritmo = class of TAlgoritmoThread;


  TIntegerIDXArray = Array of Integer;

  TGrafo = class(TInterfacedObject)
  strict private
    { Private declarations }
    FNodos: TObjectList<TNodo>;
    FSeleccionIDX: Integer;
    FID: Integer;
    FColor: Cardinal;
    FNumColumnas: Integer;
    FNumFilas: Integer;
    FOutForm: IInterfaz;
  private
    procedure OnTerminateThread(Sender: TObject);
    procedure SetColor(const Value: Cardinal);
    function GetColor: Cardinal;
  protected
    procedure InicializaMapa(AMapa: TEstructuraNodos); virtual;
    function GetOutForm: IInterfaz; virtual;
  public
    { Public declarations }
    constructor Create(AOutForm: IInterfaz; ANumFilas, ANumColumnas: Integer);
    destructor Destroy; override;
    procedure AsignaSelector(AIDXNodoActual:Integer);
    procedure BorrarNodos;
    function Coste(ANodoOrigen, ANodoDestino: TNodo): Double; virtual;
    procedure Ejecutar(AClass: TClaseAlgoritmo; ACreateSuspended: Boolean);
    function GetCoord(aIdx: Integer): TCoordPos;
    function GetDimension: Integer;
    function GetID(): Integer;
    function GetIndice(aX,aY: Integer): Integer;
    function GetNodosAdyadecentes(ANodoIDX: Integer): TIntegerIDXArray;
    function GetNodoSeleccion(): TNodo;
    function GetNodo(AIDX: Integer): TNodo;
    function Count: Integer;
    function ListaNodos: TArray<Integer>;
    function ListaNodosSalientes(ANodoOrigen: TNodo):  TArray<Integer>; virtual;
    procedure ResolverGrafo(AClass: TClass);
    function ToArray: TArrayInteger;


    class function LoadMapa(ANumFilas, ANumColumnas: Integer): TEstructuraNodos; virtual;

    property Nodos[Index: Integer]: TNodo read GetNodo;
    property SeleccionIDX: Integer read FSeleccionIDX;
    property ID: Integer read GetID;        //identificador del grafo
    property Color: Cardinal read GetColor write SetColor;
    property NumFilas: Integer read FNumFilas;        //representa el eje y
    property NumColumnas: Integer read FNumColumnas;  //representa el eje x
  end;



implementation

uses System.SysUtils, System.UITypes, System.Math, System.Generics.Defaults;

{ TGrafo }

procedure TGrafo.AsignaSelector(AIDXNodoActual:Integer);
begin
  inherited;
  FSeleccionIDX := AIDXNodoActual;
end;

procedure TGrafo.BorrarNodos;
var
  nb: TNodo;
begin
  inherited;

  for nb in FNodos do
  begin
    nb.NodoPrecedente := Nil;
  end;
end;

function TGrafo.Count: Integer;
begin
  inherited;
  Result := FNodos.Count;
end;

function TGrafo.Coste(ANodoOrigen, ANodoDestino: TNodo): Double;
begin
  Result := ANodoDestino.Coste;
end;



{
  Metodo: Create

  Descripcion: Constructor de la clase.
               Se parametriza con una referencia a la interfaz, que le
               permite as� sincronizar distintos eventos de comunicaci�n
               entre la instancia y el formulario.
               El puzzle necesita conocer durante su creaci�n las filas y
               columnas de su estructura para leer correctamente el mapa
               que va a cargar inicialmente y saber si es correcto o no
               en funci�n de su dimension ANumFilas x ANumColumnas.

}
constructor TGrafo.Create(AOutForm: IInterfaz; ANumFilas, ANumColumnas: Integer);
var
  FEstructuraMapa: TEstructuraNodos;
begin
  FNumColumnas:= ANumColumnas;
  FNumFilas:= ANumFilas;

  FOutForm := AOutForm;
  // asignaci�n o recreacion del mapa
  FEstructuraMapa:= TGrafo.LoadMapa(FNumFilas, FNumColumnas);
  FNodos:= TObjectList<TNodo>.Create(True);
  if (Length(FEstructuraMapa) * Length(FEstructuraMapa[0])) <> (FNumFilas* FNumColumnas) then
    raise Exception.Create('Error: No coincide la dimensi�n de la estructura con el numero de columnas y filas');

  InicializaMapa(FEstructuraMapa);

  FColor:= TAlphaColorRec.Black;
end;

destructor TGrafo.Destroy;
begin
  FNodos.Clear;
  FNodos.Free;
  inherited;
end;

procedure TGrafo.Ejecutar(AClass: TClaseAlgoritmo; ACreateSuspended: Boolean);
begin
  with AClass.Create(ACreateSuspended,Self, FOutForm) do
  begin
   OnTerminate:= OnTerminateThread;
   Start;
  end;
end;

function TGrafo.GetColor: Cardinal;
begin
  Result:= FColor;
end;

function TGrafo.GetCoord(aIdx: Integer): TCoordPos;
begin
  if aIdx < NumFilas then
  begin
    Result.cX:= 1;
    Result.fY:= aIdx+1;
  end
  else begin
    if ((aIdx+1) mod NumFilas) = 0 then
    begin
      Result.cX:= ((aIdx+1) div NumFilas);
      Result.fY:= NumFilas;
    end
    else
    begin
      Result.cX:= ((aIdx+1) div NumFilas)+1;
      Result.fY:= ((aIdx+1) mod NumFilas);
    end;
  end;
end;

function TGrafo.GetDimension: Integer;
begin
  Result:= FNumColumnas * FNumFilas;
end;

function TGrafo.GetOutForm: IInterfaz;
begin
  Result:= FOutForm;
end;

{
  Nota del d�a 14/09/2017:
  De cara a la version m�vil, una propuesta es sustituir el lanzamiento de la
  excepci�n por la devoluci�n de un codigo de retorno de error, como por ejemplo
  -1. En ese caso, y localizado los puntos de invocaci�n en otros modulos, podriamos
  finalizar en el hilo principal emitiendo un sonido de error (dado que no tiene
  mucho sentido el bloqueo del cuadro de di�logo).

    if (aX > NumColumnas) or (aX < 1) or (aY > NumFilas) or (aY < 1) then
      Result:= 1
    else
      Result:= (aX-1) * NumFilas + aY - 1

}

function TGrafo.GetIndice(aX, aY: Integer): Integer;
begin
  if (aX > NumColumnas) or (aX < 1) or (aY > NumFilas) or (aY < 1) then
    raise Exception.Create('Valores de rango incorrectos para X,Y ('+aX.ToString + aY.ToString+')');

  Result:= (aX-1) * NumFilas + aY - 1
end;

function TGrafo.GetNodosAdyadecentes(ANodoIDX: Integer): TIntegerIDXArray;
var
  i: Integer;
  c2d: TNodo;
begin
  c2d:= self.GetNodo(ANodoIDX) as TNodo;
  SetLength(Result, 4);
  i:= -1;
  //a la derecha
  if c2d.cX+1 <= NumColumnas then
  begin
    Inc(i);
    Result[i]:= GetIndice(c2d.cX+1,c2d.fY);
  end;
  //abajo
  if c2d.fY-1 >= 1 then
  begin
    Inc(i);
    Result[i]:= GetIndice(c2d.cX,c2d.fY-1);
  end;
  //a la izquierda
  if c2d.cX-1 >= 1 then
  begin
    Inc(i);
    Result[i]:= GetIndice(c2d.cX-1,c2d.fY);
  end;
  //arriba
  if c2d.fY+1 <= NumFilas then
  begin
    Inc(i);
    Result[i]:= GetIndice(c2d.cX,c2d.fY+1);
  end;
  SetLength(Result, i+1);
end;

procedure TGrafo.InicializaMapa(AMapa: TEstructuraNodos);
var
  i,j: Integer;
  n: TNodo;
  MaxDim1, MaxDim2: Integer;
begin
  FNodos.Clear;
  //inicializa mapa
  MaxDim1:= High(AMapa);
  for i := 0 to MaxDim1  do
  begin
    MaxDim2:= High(AMapa[i]);
    for j := 0 to MaxDim2  do
    begin
      n:= TNodo.Create(self);
      n.IDX:= FNodos.Add(n);
      n.TipoCasilla:= AMapa[i][j].Tipo;
    end;
  end;
end;

function TGrafo.ListaNodos: TArray<Integer>;
var
  n: TNodo;
  FListaNodos: TList<Integer>;
begin
  FListaNodos := TList<Integer>.Create;
  try
    for n in FNodos do
      FListaNodos.Add(n.IDX);

    Result := FListaNodos.ToArray;
  finally
    FListaNodos.Free;
  end;
end;

function TGrafo.ListaNodosSalientes(ANodoOrigen: TNodo):  TArray<Integer>;
type
  TSentidoMovimiento = (smNoDefinido = 0, smAbajo = 1, smDerecha = 2, smArriba = 3, smIzquierda = 4);
  TArrayIDXSentidos = Array[1..4] of Integer ;
var
  Lista: TList<Integer>;
  i: Integer;
  n: TNodo;
  sm: TSentidoMovimiento;
  ListaIDXAdyacentes: TIntegerIDXArray;
  ListaEnOrden: TArrayIDXSentidos;
begin
  FillChar(ListaEnOrden, 4*SizeOf(Integer), -1);
  Lista := TList<Integer>.Create;
  try
    ListaIDXAdyacentes:= GetNodosAdyadecentes(ANodoOrigen.IDX);
    for i:= 0 to Length(ListaIDXAdyacentes)-1 do
    begin
      n:= Nodos[ListaIDXAdyacentes[i]];

      if n.Accesible() then
      begin
        sm := smNoDefinido;
        if (ANodoOrigen.ALaDerechaDe(n)) then
          sm := smDerecha
        else if (ANodoOrigen.AbajoDe(n)) then
               sm := smAbajo
             else if (ANodoOrigen.ALaIzquierdaDe(n)) then
                    sm := smIzquierda
                  else if (ANodoOrigen.ArribaDe(n)) then
                         sm := smArriba;


        if sm <> smNoDefinido then
           ListaEnOrden[Integer(sm)]:= n.IDX;
      end;
    end;
    for i:= 1 to 4 do
        if ListaEnOrden[i] <> -1 then
          Lista.Add(ListaEnOrden[i]);
    Result:= Lista.ToArray;
  finally
    Lista.Free;
    Lista:= Nil;
  end;
end;

class function TGrafo.LoadMapa(ANumFilas,
  ANumColumnas: Integer): TEstructuraNodos;
begin
  Result:= [[]];
  case ANumColumnas of
    3: case ANumFilas of
         3: Result:= Get3x3Mapa;
       end;
    4: case ANumFilas of
         4: Result:= Get4x4Mapa;
       end;
    5: case ANumFilas of
         5: Result:= Get5x5Mapa;
       end;
  end;
end;

procedure TGrafo.OnTerminateThread(Sender: TObject);
begin
  //preparamos la variable para que puede interrumpir
  //el ciclo en el proximo  hilo ejecutado

end;

function TGrafo.GetNodoSeleccion: TNodo;
begin
  Result:= FNodos[FSeleccionIDX];
end;

function TGrafo.GetID: Integer;
begin
  Result:= FID;
end;

function TGrafo.GetNodo(AIDX: Integer): TNodo;
begin
  Result:= FNodos[AIDX];
end;

procedure TGrafo.ResolverGrafo(AClass: TClass);
begin
  inherited;
  Ejecutar(TClaseAlgoritmo(AClass), True);
 end;

procedure TGrafo.SetColor(const Value: Cardinal);
begin
  FColor := Value;
end;

{
  Metodo: ToArray

  Descripcion: Array del Identificador del estado del puzzle, representado por
               la tipologia de las casillas


}
function TGrafo.ToArray: TArrayInteger;
var
  i: Integer;
begin
  Result:= [];

  SetLength(Result, NumFilas*NumColumnas);

  for i:= 0 to Count-1 do
    Result[i]:= Integer(Nodos[i].TipoCasilla);

end;

constructor TAlgoritmoThread.Create(CreateSuspended:Boolean; AGrafo: TGrafo; AInterfaz: IInterfaz);
begin
  inherited Create(CreateSuspended);
  self.FreeOnTerminate:= True;
  Grafo:= AGrafo;
  Interfaz:= AInterfaz;
end;

procedure TAlgoritmoThread.Execute;
begin
  inherited;
  Resolver;
end;

procedure TAlgoritmoThread.DoActualizaNodoSelectorEnInterfaz;
begin
  if Assigned(Interfaz) then Interfaz.Update(Grafo);
end;

procedure TAlgoritmoThread.DoBorrarNodos;
begin
  Grafo.BorrarNodos;
end;

procedure TAlgoritmoThread.DoFinaliza;
begin
end;


procedure TAlgoritmoThread.Resolver;
begin
  if Assigned(Grafo) then
  begin
    DoBorrarNodos;
    Run();
  end
  else raise Exception.Create('Error al resolver el algoritmo. No exite input grafo');
end;



{ TNodoGrafo }

procedure TNodo.SetDistanciaADestino(const Value: Double);
begin
  FDistanciaADestino := Value;
end;

procedure TNodo.SetIDX(const Value: Integer);
begin
  FIDX := Value;
end;

procedure TNodo.SetNodoPrecedente(const Value: TNodo);
begin
  FNodoPrecedente := Value;
end;


function TNodo.AbajoDe(ANodo: TNodo): Boolean;
begin
  //AbajoDe
  Result:= (cX = ANodo.cX) and
           (fY = ANodo.fY - 1);
end;

function TNodo.Accesible: Boolean;
begin
   Result:= True;
end;

function TNodo.ALaDerechaDe(ANodo: TNodo): Boolean;
begin
  //ALaDerechaDe
  Result:= (cX = ANodo.cX + 1) and
           (fY = ANodo.fY);
end;

function TNodo.ALaIzquierdaDe(ANodo: TNodo): Boolean;
begin
  //ALaIzquierdaDe
  Result:= (cX = ANodo.cX - 1) and
           (fY = ANodo.fY);
end;

function TNodo.ArribaDe(ANodo: TNodo): Boolean;
begin
  //ArribaDe
  Result:= (cX = ANodo.cX) and
           (fY = ANodo.fY + 1);
end;

function TNodo.Clonar: TNodo;
var
 c2d: TNodo;
begin
  c2d:= TNodo.Create(Grafo);
  c2d.IDX:= self.IDX;
  c2d.TipoCasilla:= self.FTipoCasilla;
  c2d.NodoPrecedente:= self.NodoPrecedente;
  Result:= c2d;
end;

function TNodo.GetCoste: Single;
begin
   case FTipoCasilla of
       tc0        : Result:= System.Math.Infinity;
   else
       Result        := 1;
   end;
end;

constructor TNodo.Create(AGrafo: TGrafo);
begin
  FNodoPrecedente:= Nil;
  FDistanciaADestino:= 0;
  Grafo:= AGrafo;
end;

destructor TNodo.Destroy;
begin
  Grafo:= Nil;
  inherited;
end;

function TNodo.GetIDX: Integer;
begin
  Result:= FIDX;
end;

function TNodo.GetInternalcX: Integer;
begin
  Result:=  Grafo.GetCoord(IDX).cX;
end;

function TNodo.GetInternalfY: Integer;
begin
  Result:= Grafo.GetCoord(IDX).fY;
end;

function TNodo.GetTipoCasilla: TTipoCasilla;
begin
  Result:= FTipoCasilla;
end;

procedure TNodo.SetTipoCasilla(ATipoCasilla: TTipoCasilla);
begin
  FTipoCasilla:= ATipoCasilla;
end;


{
  Convierte la casilla en la estructura de coordenadas
  Ayuda para recrear el mapa en una array de tipo TEstructuraMapa
}
function TNodo.ToCoordPos: TCoordPos;
begin
  Result:= TCoordPos.Create(self.cX, self.fY, self.FTipoCasilla);
end;

function TNodo.ToString: String;
begin
   Result:= '[(' + GetInternalcX.ToString + ';' + GetInternalfY.ToString + ');' + TextoTipoCasilla[TipoCasilla] + ']';
end;


end.
