program PuzzleAndroid;

uses
  System.StartUpCopy,
  FMX.Forms,
  HeaderFooterTemplate in 'HeaderFooterTemplate.pas' {HeaderFooterForm},
  UClassesHelper in '..\Helper\UClassesHelper.pas',
  UClaseAlgoritmo8PuzzleBlank in '..\Algoritmos\UClaseAlgoritmo8PuzzleBlank.pas',
  UClaseEscenario in '..\Marco\UClaseEscenario.pas',
  UClaseGrafo in '..\Marco\UClaseGrafo.pas',
  UClasePuzzle in '..\Marco\UClasePuzzle.pas',
  UClaseRegistro in '..\Marco\UClaseRegistro.pas',
  UTipos in '..\Marco\UTipos.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(THeaderFooterForm, HeaderFooterForm);
  Application.Run;
end.
