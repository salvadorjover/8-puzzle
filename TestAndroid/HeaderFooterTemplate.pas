unit HeaderFooterTemplate;

{
*******************************************************************************
Version 2017.9.16.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

  El blog de Delphi B�sico no tiene �nimo de lucro y su fin es el de colaborar
  a la divulgaci�n de conocimientos dentro de la comunidad de programadores de
  Delphi, desde su nacimiento en el a�o 2003.

  ******************************************************************************
  ******************************************************************************
  ******************************************************************************

  EL SOFTWARE SE PROPORCIONA "TAL CUAL", SIN GARANT�A DE NING�N TIPO, EXPRESA O
  IMPL�CITA, INCLUYENDO PERO NO LIMITADO A GARANT�AS DE COMERCIALIZACI�N, IDONEIDAD
  PARA UN PROP�SITO PARTICULAR Y NO INFRACCI�N. EN NING�N CASO LOS AUTORES O
  TITULARES DEL COPYRIGHT SER�N RESPONSABLES DE NINGUNA RECLAMACI�N, DA�OS U
  OTRAS RESPONSABILIDADES, YA SEA EN UNA ACCI�N DE CONTRATO, AGRAVIO O CUALQUIER
  OTRO MOTIVO, QUE SURJA DE O EN CONEXI�N CON EL SOFTWARE O EL USO U OTRO TIPO
  DE ACCIONES EN EL SOFTWARE.

  ******************************************************************************
  ******************************************************************************
  ******************************************************************************


Unidad: Interfaz principal.
          -Aplicable a varias configuraciones 3x3, 4x4, 5x5, etc... en funcion
           del mapa cargado.
          -La carga del mapa por defecto se toma como solucion u objetivo.
          -Permite mover una casilla o varias (clic en el tablero).
          -Genera movimientos aleatorios para la posici�n inicial
          -Inicializa la posici�n de partida (solucion).
          -Ejecuta la resoluci�n del problema. (**)
          -Visualiza los pasos de resolucion del problema
          -Permite la gesti�n de una imagen en sustituci�n del identficador
           num�rico.


}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  UClaseGrafo, UClasePuzzle, FMX.Objects, FMX.Layouts, FMX.Controls.Presentation,
  UClaseEscenario, System.Actions, FMX.ActnList, FMX.ListBox, FMX.StdActns;

const
  kTestImgName   = 'test.jpg';
  KNumMezclas    = 1000;


type
  THeaderFooterForm = class(TForm, IInterfaz)
    Header: TToolBar;
    Footer: TToolBar;
    HeaderLabel: TLabel;
    MarcoPuzzle: TLayout;
    pbxPuzzle: TPaintBox;
    ImgSol: TImage;
    Button1: TButton;
    Button2: TButton;
    acciones: TActionList;
    acMezclar: TAction;
    acReset: TAction;
    cbxTipoPuzzle: TComboBox;
    MarcoImagen: TLayout;
    lbContador: TLabel;
    tmContadorTiempo: TTimer;
    lbTiempo: TLabel;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Rectangle1: TRectangle;
    Layout4: TLayout;
    WindowClose1: TWindowClose;
    procedure FormCreate(Sender: TObject);
    procedure pbxPuzzlePaint(Sender: TObject; Canvas: TCanvas);
    procedure acMezclarExecute(Sender: TObject);
    procedure acMezclarUpdate(Sender: TObject);
    procedure acResetExecute(Sender: TObject);
    procedure pbxPuzzleClick(Sender: TObject);
    procedure pbxPuzzleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure cbxTipoPuzzleChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tmContadorTiempoTimer(Sender: TObject);
    procedure acResetUpdate(Sender: TObject);
  strict private
    FEnJuego: Boolean;
    FInicio: TDateTime;
  private
    { Private declarations }
    FImagen: TBitmap;
    FNumMezclas: Integer;
    FPoint: TPointF;
    FModuleEscenario: TClaseEscenario;
    Puzzle: TPuzzle;
    procedure ActualizaInterfaz;
    function GetTiempoTranscurrido(ANow, AThen: TDateTime): String;
    procedure JuegoIniciar;
    procedure JuegoFinalizar;
    procedure Update(const AGrafo: TGrafo);
    procedure FinalizaAlgoritmo(const AGrafo: TGrafo; AFinishDateTime: TDateTime; APasosResolucion: TArrayOfArrayInteger);
  public
    { Public declarations }
  end;

var
  HeaderFooterForm: THeaderFooterForm;

implementation

{$R *.fmx}

uses UClassesHelper, UTipos, System.IOUtils, System.DateUtils;


procedure THeaderFooterForm.FormCreate(Sender: TObject);
const
  kNumFilas = 3;
  kNumColumnas = 3;
var
  s: String;
begin
  FModuleEscenario:= TClaseEscenario.Create(kNumFilas, kNumColumnas);
  Puzzle:= FModuleEscenario.Puzzles[FModuleEscenario.AddPuzzle(TPuzzle.Create(Self, kNumFilas, kNumColumnas))];
  Puzzle.Color:= TAlphaColorRec.Blue;
  Puzzle.AsignaSelector(Puzzle.GetIDXCero);
  pbxPuzzle.Enabled:= False;
  FNumMezclas:= KNumMezclas;

{$IFDEF ANDROID}
  s:= TPath.GetDocumentsPath + PathDelim + kTestImgName;
{$ENDIF}

{$IFDEF MSWINDOWS}
  s:= kTestImgName;
{$ENDIF}


  if FileExists(s) then
  begin
    fImagen:= TBitmap.Create;
    fImagen.LoadFromFile(s);
    imgSol.Bitmap.LoadFromFile(s);
  end;
end;


procedure THeaderFooterForm.acMezclarExecute(Sender: TObject);
begin
 FNumMezclas:= KNumMezclas;
 Puzzle.Mezclar(FNumMezclas);
 Puzzle.CalcularDistanciaManhatan;
 JuegoIniciar;

 ActualizaInterfaz;
end;

procedure THeaderFooterForm.acMezclarUpdate(Sender: TObject);
begin
  acMezclar.Enabled:= not FEnJuego;
end;

procedure THeaderFooterForm.acResetExecute(Sender: TObject);
var
 nIDX: Integer;
begin
 JuegoFinalizar;

 Puzzle.Reset;
 nIDX:= Puzzle.GetIDXCero;
 if nIDX <> -1 then
   Puzzle.AsignaSelector(nIDX);
 ActualizaInterfaz;
end;

procedure THeaderFooterForm.acResetUpdate(Sender: TObject);
begin
  acReset.Enabled:= FEnJuego;
end;

procedure THeaderFooterForm.ActualizaInterfaz;
begin
  HeaderFooterForm.Invalidate;
end;

procedure THeaderFooterForm.cbxTipoPuzzleChange(Sender: TObject);
var
  kNumFilas: Integer;
  kNumColumnas: Integer;
begin
  case cbxTipoPuzzle.ItemIndex of
    0: begin
        kNumFilas:=3;
        kNumColumnas:=3;
       end;
    1: begin
        kNumFilas:=4;
        kNumColumnas:=4;
       end;
    2: begin
        kNumFilas:=5;
        kNumColumnas:=5;
       end;
    else
        raise Exception.Create('La opci�n no es v�lida...');
  end;
  if Assigned(FModuleEscenario) then FModuleEscenario.Free;
  FModuleEscenario:= TClaseEscenario.Create(kNumFilas, kNumColumnas);
  Puzzle:= FModuleEscenario.Puzzles[FModuleEscenario.AddPuzzle(TPuzzle.Create(Self, kNumFilas, kNumColumnas))];
  Puzzle.Color:= TAlphaColorRec.Blue;
  Puzzle.AsignaSelector(Puzzle.GetIDXCero);
  acResetExecute(nil);

  ActualizaInterfaz;
end;

procedure THeaderFooterForm.FinalizaAlgoritmo(const AGrafo: TGrafo;
  AFinishDateTime: TDateTime; APasosResolucion: TArrayOfArrayInteger);
begin
end;


procedure THeaderFooterForm.FormDestroy(Sender: TObject);
begin
  FImagen.Free;
  FModuleEscenario.Free;
end;

function THeaderFooterForm.GetTiempoTranscurrido(ANow,
  AThen: TDateTime): String;
var
  h, m, s: Int64;
begin

  h:= HoursBetween(Now, FInicio);
  m:= MinutesBetween(Now, FInicio);
  s:= SecondsBetween(Now, FInicio);

  if s >= 60 then s:= s mod 60;
  if m >= 60 then m:= m mod 60;


  Result:= Format(Format('%%.%dd', [2]), [h])+ ':'+
                  Format(Format('%%.%dd', [2]), [m])+ ':'+
                  Format(Format('%%.%dd', [2]), [s]);

end;


procedure THeaderFooterForm.JuegoFinalizar;
begin
  FEnJuego:= False;
  tmContadorTiempo.Enabled:= False;
  pbxPuzzle.Enabled:= False;
   cbxTipoPuzzle.Enabled:= True;
  lbTiempo.Text:= '00:00:00';
  lbContador.Text:= '0';
end;

procedure THeaderFooterForm.JuegoIniciar;
begin
   FEnJuego:= True;
   FInicio:= Now();
   tmContadorTiempo.Enabled:= True;
   pbxPuzzle.Enabled:= True;
   cbxTipoPuzzle.Enabled:= False;
   tmContadorTiempoTimer(nil);
end;

procedure THeaderFooterForm.pbxPuzzleClick(Sender: TObject);
var
 cIDX: Integer;
 cX, cY: Integer;
begin
  cX:= pbxPuzzle.GetCoordX(FPoint.X,Puzzle.NumColumnas, Puzzle.NumColumnas);
  cY:= pbxPuzzle.GetCoordY(FPoint.Y, Puzzle.NumFilas, Puzzle.NumColumnas);
  cIDX:= Puzzle.GetIndice(cX, cY);
  Puzzle.Mover(cIDX);
  ActualizaInterfaz;

  if Puzzle.EvaluarExitoMovimiento then
  begin
    tmContadorTiempo.Enabled:= False;
    ShowMessage('El problema ha sido resuelto en un tiempo de [hh:mm:ss] '+  GetTiempoTranscurrido(Now, FInicio));
  end;
end;


procedure THeaderFooterForm.pbxPuzzleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  FPoint.X:= X;
  FPoint.Y:= Y;
end;

procedure THeaderFooterForm.pbxPuzzlePaint(Sender: TObject; Canvas: TCanvas);
var
  X,Y: Integer;
  r, ri: TRectF;
  i,j: Integer;
  sol: TArrayInteger;
begin
  Canvas.BeginScene();

  Canvas.Stroke.Dash:= TStrokeDash.Dash;

{$region 'DIBUJO CASILLA SELECCION'}
    //dibujamos seleccion A
  X:=Puzzle.GetNodo(Puzzle.GetNodoSeleccion.IDX).cX;
  Y:= Puzzle.GetNodo(Puzzle.GetNodoSeleccion.IDX).fY;

  r:= pbxPuzzle.GetXYRect(X,Y, FModuleEscenario.Filas, FModuleEscenario.Columnas);
  r.Inflate(-4,-4);

  Canvas.Stroke.Color:= Puzzle.Color;
  Canvas.Stroke.Thickness:= 3;
  Canvas.DrawRect(r, 0,0, [],1, TCornerType.InnerLine);

{$endregion}

  Canvas.Stroke.Color:= TAlphaColorRec.Black;
  Canvas.Stroke.Dash:= TStrokeDash.Solid;
  Canvas.Stroke.Thickness:= 1;
  Canvas.Fill.Color:= TAlphaColorRec.Black;

{$region 'DIBUJO MAPA Y CASILLAS'}
  //dibujamos estructura mapa
  for i := 0 to Puzzle.Count-1 do
  begin
    X:= Puzzle.GetNodo(i).cX;
    Y:= Puzzle.GetNodo(i).fY;

    r:= pbxPuzzle.GetXYRect(X,Y, FModuleEscenario.Filas, FModuleEscenario.Columnas);

    Canvas.Font.Size:=18;
    Canvas.Font.Family:='Arial';
    Canvas.Fill.Color:= TAlphaColorRec.Black;
    Canvas.Font.Style:=[];
    Canvas.DrawRect(r, 0,0, [],1, TCornerType.InnerLine);
    if Puzzle.GetNodo(i).TipoCasilla <> tc0 then
      Canvas.FillText(r, Integer(Puzzle.GetNodo(i).TipoCasilla).ToString() , False, 1, [], TTextAlign.Center, TTextAlign.Center)
    else Canvas.FillText(r, '' , False, 1, [], TTextAlign.Center, TTextAlign.Center);

    //Pintado de la imagen test
    if Assigned(fImagen) then
    begin
      sol:= Puzzle.GetSolucion;
      for j:=Low(sol) to High(sol) do
      begin
        if sol[j] = Integer(Puzzle.GetNodo(i).TipoCasilla) then
        begin
         ri:= fImagen.GetXYRect(Puzzle.GetNodo(j).cX,Puzzle.GetNodo(j).fY, FModuleEscenario.Filas, FModuleEscenario.Columnas);
         if Puzzle.GetNodo(i).TipoCasilla = tc0 then
           Canvas.DrawBitmap(fImagen, ri, r, 0.2, False)
         else
           Canvas.DrawBitmap(fImagen, ri, r, 1, False);
        end;
      end;
    end;

  end;
{$endregion}

  Canvas.EndScene;
end;

procedure THeaderFooterForm.tmContadorTiempoTimer(Sender: TObject);
begin
  lbTiempo.Text:= GetTiempoTranscurrido(Now, FInicio);
  lbContador.Text:= Puzzle.Coste.ToString();
end;

procedure THeaderFooterForm.Update(const AGrafo: TGrafo);
begin
//
end;

initialization
 ReportMemoryLeaksOnShutdown := True;

end.
