unit UMainGame;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

  El blog de Delphi B�sico no tiene �nimo de lucro y su fin es el de colaborar
  a la divulgaci�n de conocimientos dentro de la comunidad de programadores de
  Delphi, desde su nacimiento en el a�o 2003.

  ******************************************************************************
  ******************************************************************************
  ******************************************************************************

  EL SOFTWARE SE PROPORCIONA "TAL CUAL", SIN GARANT�A DE NING�N TIPO, EXPRESA O
  IMPL�CITA, INCLUYENDO PERO NO LIMITADO A GARANT�AS DE COMERCIALIZACI�N, IDONEIDAD
  PARA UN PROP�SITO PARTICULAR Y NO INFRACCI�N. EN NING�N CASO LOS AUTORES O
  TITULARES DEL COPYRIGHT SER�N RESPONSABLES DE NINGUNA RECLAMACI�N, DA�OS U
  OTRAS RESPONSABILIDADES, YA SEA EN UNA ACCI�N DE CONTRATO, AGRAVIO O CUALQUIER
  OTRO MOTIVO, QUE SURJA DE O EN CONEXI�N CON EL SOFTWARE O EL USO U OTRO TIPO
  DE ACCIONES EN EL SOFTWARE.

  ******************************************************************************
  ******************************************************************************
  ******************************************************************************


Unidad: Interfaz principal.
          -Aplicable a varias configuraciones 3x3, 4x4, 5x5, etc... en funcion
           del mapa cargado.
          -La carga del mapa por defecto se toma como solucion u objetivo.
          -Permite mover una casilla o varias (clic en el tablero).
          -Genera movimientos aleatorios para la posici�n inicial
          -Inicializa la posici�n de partida (solucion).
          -Ejecuta la resoluci�n del problema. (**)
          -Visualiza los pasos de resolucion del problema
          -Permite la gesti�n de una imagen en sustituci�n del identficador
           num�rico.

          * No advierte de problema no resoluble puesto que por
          definici�n siempre tendr�a soluci�n, dado que los moviemientos
          aleatorios se generan siempre de forma l�cita y no es posible
          alterar el tablero a una posici�n no consistente o irresoluble.

         ** Se propone Algoritmo A* (AStar) para ver de forma separada en un
            Taller del Blog de Delphi B�sico junto con iniciativas alternativas
            de los participantes. El codigo de dicha unidad se veria dentro de
            dicho taller.
}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  UClaseEscenario, FMX.Controls.Presentation, FMX.StdCtrls, UClasePuzzle,
  UClaseGrafo, FMX.ListBox, FMX.Layouts, FMX.Edit, System.Actions, FMX.ActnList;

const
  kTestImgName   = 'test.jpg';

  {
    Ver unidades en carpeta Algoritmos en Codigo Fuente
       * Plantilla vacia = TAlgoritmo8puzzleBlank
       * Algoritmo a*    = TAlgoritmo8puzzle      - ver taller de grupo}

  kTestAlgortimo = 'TAlgoritmo8puzzleBlank';

type
  Tfrm8puzzle = class(TForm, IInterfaz)
    pnCabecera: TPanel;
    btnResolver: TButton;
    btnMezclar: TButton;
    pbxPuzzle: TPaintBox;
    lbEtiquetaMezclas: TLabel;
    btnReset: TButton;
    lbEtiquetaSimulaciones: TLabel;
    lbContadorSimulaciones: TLabel;
    lbInfoPuzzleActual: TLabel;
    cbxTipoPuzzle: TComboBox;
    lbEtiquetaSolucion: TLabel;
    lbContadorSoluciones: TLabel;
    pnResultados: TPanel;
    lbxResoluciones: TListBox;
    pnCabeceraResultados: TPanel;
    lbEtiquetaRutaMovimientos: TLabel;
    lbInfoPuzleInicial: TLabel;
    lbContadorMezclas: TLabel;
    lbEtiquetaSeleccionTipo: TLabel;
    lbEtiquetaSeleccionMezclas: TLabel;
    cbxMezclas: TComboBox;
    trbTransparencia: TTrackBar;
    Panel1: TPanel;
    Label1: TLabel;
    imgSol: TImage;
    lbVisible: TLabel;
    acciones: TActionList;
    acResolver: TAction;
    acMezclar: TAction;
    acReset: TAction;
    procedure FormCreate(Sender: TObject);
    procedure rcTableroResize(Sender: TObject);
    procedure pbxPuzzlePaint(Sender: TObject; Canvas: TCanvas);
    procedure pbxPuzzleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure pbxPuzzleClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pbxPuzzleMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure cbxTipoPuzzleChange(Sender: TObject);
    procedure lbxResolucionesChange(Sender: TObject);
    procedure trbTransparenciaChange(Sender: TObject);
    procedure acResolverExecute(Sender: TObject);
    procedure acResolverUpdate(Sender: TObject);
    procedure acMezclarExecute(Sender: TObject);
    procedure acMezclarUpdate(Sender: TObject);
    procedure acResetExecute(Sender: TObject);
  private
    { Private declarations }
    FImagen: TBitmap;
    FEnProceso: Boolean;
    FNumMezclas: Integer;
    FPoint: TPointF;
    FModuleEscenario: TClaseEscenario;
    Puzzle: TPuzzle;
    procedure ActualizaInterfaz;
    procedure LimpiaPasosResolucion;
    procedure Update(const AGrafo: TGrafo);
    procedure FinalizaAlgoritmo(const AGrafo: TGrafo; AFinishDateTime: TDateTime; APasosResolucion: TArrayOfArrayInteger);
  public
    { Public declarations }
  end;

var
  frm8puzzle: Tfrm8puzzle;

implementation

{$R *.fmx}

uses UClassesHelper, UTipos, UClaseRegistro;


procedure Tfrm8puzzle.acMezclarExecute(Sender: TObject);
begin
 FNumMezclas:= cbxMezclas.Items[cbxMezclas.ItemIndex].ToInteger;
 Puzzle.Mezclar(FNumMezclas);
 Puzzle.CalcularDistanciaManhatan;
 ActualizaInterfaz;
end;

procedure Tfrm8puzzle.acMezclarUpdate(Sender: TObject);
begin
  acMezclar.Enabled:= not FEnProceso;
end;

procedure Tfrm8puzzle.acResetExecute(Sender: TObject);
var
 nIDX: Integer;
begin
 FEnProceso:= False;
 Puzzle.Reset;
 nIDX:= Puzzle.GetIDXCero;
 if nIDX <> -1 then
   Puzzle.AsignaSelector(nIDX);
 ActualizaInterfaz;
end;

procedure Tfrm8puzzle.acResolverExecute(Sender: TObject);
begin
  lbInfoPuzleInicial.Text:= 'Situacion de Partida: ' + Puzzle.ToString;
  LimpiaPasosResolucion;
  FEnProceso:= True;
  Puzzle.ResolverGrafo(ModuleInfoManager.GetModuleClassByName(kTestAlgortimo));

  ActualizaInterfaz;
end;

procedure Tfrm8puzzle.acResolverUpdate(Sender: TObject);
begin
  acResolver.Enabled:= not FEnProceso;
end;

procedure Tfrm8puzzle.ActualizaInterfaz;
begin
  frm8puzzle.Invalidate;
  if Puzzle <> Nil then
  begin
    lbContadorSimulaciones.Text:= Puzzle.Coste.TOString();
    lbContadorMezclas.Text:= FNumMezclas.ToString();
    lbInfoPuzzleActual.Text:= '  Grafo: '+ Puzzle.ToString +
                              '         D.M.: '+ Puzzle.DistanciaManhatan.ToString +
                              '         Desplazadas: '+ Puzzle.Desplazadas.ToString;
    lbContadorSoluciones.Text:= lbxResoluciones.Count.ToString;
  end;
  cbxTipoPuzzle.Enabled:= not FEnProceso;
end;

procedure Tfrm8puzzle.cbxTipoPuzzleChange(Sender: TObject);
var
  kNumFilas: Integer;
  kNumColumnas: Integer;
begin
  case cbxTipoPuzzle.ItemIndex of
    0: begin
        kNumFilas:=3;
        kNumColumnas:=3;
       end;
    1: begin
        kNumFilas:=4;
        kNumColumnas:=4;
       end;
    2: begin
        kNumFilas:=5;
        kNumColumnas:=5;
       end;
    else
        raise Exception.Create('La opci�n no es v�lida...');
  end;
  if Assigned(FModuleEscenario) then FModuleEscenario.Free;
  FModuleEscenario:= TClaseEscenario.Create(kNumFilas, kNumColumnas);
  Puzzle:= FModuleEscenario.Puzzles[FModuleEscenario.AddPuzzle(TPuzzle.Create(Self, kNumFilas, kNumColumnas))];
  Puzzle.Color:= TAlphaColorRec.Blue;
  Puzzle.AsignaSelector(Puzzle.GetIDXCero);
  ActualizaInterfaz;
end;

procedure Tfrm8puzzle.FinalizaAlgoritmo(const AGrafo: TGrafo;
  AFinishDateTime: TDateTime; APasosResolucion: TArrayOfArrayInteger);
 var
   iElementos: Integer;
   i: Integer;
   Bound: TBoundArray;
   ai: TArrayInteger;
   P: TPuzzle;
begin
  Bound := DynArrayBounds(Pointer(APasosResolucion), TypeInfo(TArrayOfArrayInteger));

  iElementos:= Bound[0];
  for I := 0 to iElementos do
  begin
      SetLength(ai, AGrafo.Count);
      ai:= APasosResolucion[I];
      P:= Puzzle.Clonar;
      P.Copiar(ai);

        case I of
          0: lbxResoluciones.Items.AddObject('Inicio-> '+P.ToString, P);
        else
          if i = iElementos then
            lbxResoluciones.Items.AddObject('Fin->'+P.ToString,  P)
          else
            lbxResoluciones.Items.AddObject('Mover->'+P.ToString,  P);
        end;
  end;

  if FEnProceso then ShowMessage('El problema ha sido resuelto.');
  FEnProceso:= False;
  ActualizaInterfaz;
end;

procedure Tfrm8puzzle.pbxPuzzleClick(Sender: TObject);
var
 cIDX: Integer;
 cX, cY: Integer;
begin
  cX:= pbxPuzzle.GetCoordX(FPoint.X, Puzzle.NumFilas, Puzzle.NumColumnas);
  cY:= pbxPuzzle.GetCoordY(FPoint.Y, Puzzle.NumFilas, Puzzle.NumColumnas);
  cIDX:= Puzzle.GetIndice(cX, cY);
  lbContadorMezclas.Text:= cIDX.ToString();
  Puzzle.Mover(cIDX);
  ActualizaInterfaz;
  if Puzzle.EvaluarExitoMovimiento and (not FEnProceso) then
     ShowMessage('El problema ha sido resuelto.');
end;

procedure Tfrm8puzzle.pbxPuzzleMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FPoint.X:= X;
  FPoint.Y:= Y;
end;

procedure Tfrm8puzzle.pbxPuzzleMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Single);
var
 cIDX: Integer;
 cX, cY: Integer;
begin
    FPoint.X:= X;
    FPoint.Y:= Y;
    cX:= pbxPuzzle.GetCoordX(FPoint.X, Puzzle.NumFilas, Puzzle.NumColumnas);
    cY:= pbxPuzzle.GetCoordY(FPoint.Y, Puzzle.NumFilas, Puzzle.NumColumnas);
    if (cX >= 1) and (cx <= Puzzle.NumColumnas) and
       (cY >= 1) and (cx <= Puzzle.NumFilas)  then
    begin
      cIDX:= Puzzle.GetIndice(cX, cY);
      lbInfoPuzzleActual.Text:= 'Altura (H) '+ pbxPuzzle.Height.ToString +
                                ' X Ancho (W) '+ pbxPuzzle.Width.ToString +
                                '   XY('+ X.ToString + ','+ Y.ToString + ')   C('+cX.ToString + ','+ cY.ToString + ')=  IDX:'+ cIDX.ToString();
    end;
end;

procedure Tfrm8puzzle.pbxPuzzlePaint(Sender: TObject; Canvas: TCanvas);
var
  X,Y: Integer;
  r, ri: TRectF;
  i,j: Integer;
  sol: TArrayInteger;
begin
  Canvas.BeginScene();

  Canvas.Stroke.Dash:= TStrokeDash.Dash;

{$region 'DIBUJO CASILLA SELECCION'}
    //dibujamos seleccion A
  X:=Puzzle.GetNodo(Puzzle.GetNodoSeleccion.IDX).cX;
  Y:= Puzzle.GetNodo(Puzzle.GetNodoSeleccion.IDX).fY;

  r:= pbxPuzzle.GetXYRect(X,Y, FModuleEscenario.Filas, FModuleEscenario.Columnas);
  r.Inflate(-4,-4);

  Canvas.Stroke.Color:= Puzzle.Color;
  Canvas.Stroke.Thickness:= 3;
  Canvas.DrawRect(r, 0,0, [],1, TCornerType.InnerLine);

{$endregion}

  Canvas.Stroke.Color:= TAlphaColorRec.Black;
  Canvas.Stroke.Dash:= TStrokeDash.Solid;
  Canvas.Stroke.Thickness:= 1;
  Canvas.Fill.Color:= TAlphaColorRec.Black;

{$region 'DIBUJO MAPA Y CASILLAS'}
  //dibujamos estructura mapa
  for i := 0 to Puzzle.Count-1 do
  begin
    X:= Puzzle.GetNodo(i).cX;
    Y:= Puzzle.GetNodo(i).fY;

    r:= pbxPuzzle.GetXYRect(X,Y, FModuleEscenario.Filas, FModuleEscenario.Columnas);

    Canvas.Font.Size:=18;
    Canvas.Font.Family:='Arial';
    Canvas.Fill.Color:= TAlphaColorRec.Black;
    Canvas.Font.Style:=[];
    Canvas.DrawRect(r, 0,0, [],1, TCornerType.InnerLine);
    if Puzzle.GetNodo(i).TipoCasilla <> tc0 then
      Canvas.FillText(r, Integer(Puzzle.GetNodo(i).TipoCasilla).ToString() , False, 1, [], TTextAlign.Center, TTextAlign.Center)
    else Canvas.FillText(r, '' , False, 1, [], TTextAlign.Center, TTextAlign.Center);

    //Pintado de la imagen test
    if Assigned(fImagen) then
    begin
      sol:= Puzzle.GetSolucion;
      for j:=Low(sol) to High(sol) do
      begin
        if sol[j] = Integer(Puzzle.GetNodo(i).TipoCasilla) then
        begin
         ri:= fImagen.GetXYRect(Puzzle.GetNodo(j).cX,Puzzle.GetNodo(j).fY, FModuleEscenario.Filas, FModuleEscenario.Columnas);
         if Puzzle.GetNodo(i).TipoCasilla = tc0 then
           Canvas.DrawBitmap(fImagen, ri, r, trbTransparencia.Value, False)
         else
           Canvas.DrawBitmap(fImagen, ri, r, 0.5+trbTransparencia.Value, False);
        end;
      end;
    end;

  end;
{$endregion}

  Canvas.EndScene;
end;

procedure Tfrm8puzzle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LimpiaPasosResolucion;
  FImagen.Free;
  FModuleEscenario.Free;
end;

procedure Tfrm8puzzle.FormCreate(Sender: TObject);
const
  kNumFilas = 3;
  kNumColumnas = 3;
begin
  FModuleEscenario:= TClaseEscenario.Create(kNumFilas, kNumColumnas);
  Puzzle:= FModuleEscenario.Puzzles[FModuleEscenario.AddPuzzle(TPuzzle.Create(Self, kNumFilas, kNumColumnas))];
  Puzzle.Color:= TAlphaColorRec.Blue;
  Puzzle.AsignaSelector(Puzzle.GetIDXCero);

  if FileExists(kTestImgName) then
  begin
    fImagen:= TBitmap.Create;
    fImagen.LoadFromFile(kTestImgName);
    imgSol.Bitmap.LoadFromFile(kTestImgName);
  end;
end;

procedure Tfrm8puzzle.rcTableroResize(Sender: TObject);
begin
  ActualizaInterfaz;
end;

procedure Tfrm8puzzle.trbTransparenciaChange(Sender: TObject);
begin
  ActualizaInterfaz;
end;

procedure Tfrm8puzzle.Update(const AGrafo: TGrafo);
begin
  ActualizaInterfaz;
  {Sleep(0);}
  Application.ProcessMessages;
end;


procedure Tfrm8puzzle.lbxResolucionesChange(Sender: TObject);
var
 P: TPuzzle;
begin
  if (lbxResoluciones.Count > 0) and (lbxResoluciones.ItemIndex <> -1) then
  begin
    P:= lbxResoluciones.Items.Objects[lbxResoluciones.ItemIndex] as TPuzzle;
    Puzzle.Copiar(P.ToArray);
  end;
  Puzzle.AsignaSelector(Puzzle.GetIDXCero);
  ActualizaInterfaz;
end;

procedure Tfrm8puzzle.LimpiaPasosResolucion;
var
  P: TPuzzle;
  I: Integer;
begin
  for I := 0 to lbxResoluciones.Count-1 do
    lbxResoluciones.Items.Objects[I].Free;

  lbxResoluciones.Clear;
end;

initialization
 ReportMemoryLeaksOnShutdown := True;


end.
