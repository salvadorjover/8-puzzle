program Play8Puzzle;

uses
  System.StartUpCopy,
  FMX.Forms,
  UMainGame in 'UMainGame.pas' {frm8puzzle},
  UClassesHelper in '..\Helper\UClassesHelper.pas',
  UClaseEscenario in '..\Marco\UClaseEscenario.pas',
  UClaseGrafo in '..\Marco\UClaseGrafo.pas',
  UClasePuzzle in '..\Marco\UClasePuzzle.pas',
  UTipos in '..\Marco\UTipos.pas',
  UClassesLog in '..\Algoritmos\UClassesLog.pas',
  UClaseAlgoritmo8PuzzleBlank in '..\Algoritmos\UClaseAlgoritmo8PuzzleBlank.pas',
  UClaseRegistro in '..\Marco\UClaseRegistro.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm8puzzle, frm8puzzle);
  Application.Run;
end.
