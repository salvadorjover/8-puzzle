unit UClassesHelper;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClassesHelper.pas

Objetivo: Extiende la funcionalidad de las clases para que sean capaces de
          calcular dos aspectos relacionados intimamente con la cuadricula.
          Por un lado necesitamos convertir un punto de la cuadricula en una
          coordenada, de forma que al pulsar el usuario sobre la misma, seamos
          capaces de saber sobre que coordenada se est� actuando. Para ello,
          nos apoyamos en la clase TTranslatorCoord, que convierte un punto del
          tablero en una coordenada.
          Por otro lado, dada una coordenada cualquiera del tablero, necesitamos
          saber para dicho contenedor, cual es el area de la misma, representada
          en el registro TRectF. Para ello, la clase TTranslatorPoint expone
          el m�todo GetXYRect.
          Estas dos funcionalidades, nos ayudan por un lado para saber que ficha
          debe mover en funcion del punto en el que el usuario ha pulsado. Y por
          otro lado, en el caso de la segunda, nos ayudar�n a conocer el area de
          pintado, para personalizar la cuadricula segun el caso.

          Nota: La frecuencia representa la amplitud de la cuadricula.
}

interface

uses
  System.Types, FMX.Objects, FMX.Graphics;


type

  TPaintBoxHelper = class helper for TPaintBox
  private
    function GetFrecuency(ANumFilas, ANumColumnas: Integer): Single;
  protected
    function GetXYPoint(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TPointF;
  published
    function GetXYRect(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TRectF;
    function GetCoordX(XPoint: Single; ANumFilas, ANumColumnas: Integer): Integer;
    function GetCoordY(YPoint: Single; ANumFilas, ANumColumnas: Integer): Integer;
  end;

  TBitmapHelper = class helper for TBitmap
  private
    function GetFrecuency(ANumFilas, ANumColumnas: Integer): Single;
  protected
    function GetXYPoint(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TPointF;
  published
    function GetXYRect(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TRectF;
  end;

  TTranslatorCoord = class
  public
    class function GetCoordX(AWidth: Single; XPoint: Single; ANumColumnas: Integer; AFrecuency: Single = 100.0): Integer;
    class function GetCoordY(AHeight: Single; YPoint: Single; ANumFilas: Integer; AFrecuency: Single = 100.0): Integer;
  end;

  TTranslatorPoint = class
  protected
    class function GetXYPoint(AHeight: Single;  AcX, AcY: Integer; AFrecuency: Single = 100.0): TPointF;
  public
    class function GetXYRect(AHeight, AWidth: Single; AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer; AFrecuency: Single = 100.0): TRectF;
  end;


implementation


function GetMaxDim(AHeight, AWidth: Single; ANumFilas, ANumColumnas: Integer): Single;
var
  dim: Single;
  num: Integer;
begin
   if AHeight > AWidth then
   begin
    dim:= AWidth;
    num:= ANumColumnas;
   end
   else
   begin
     dim:= AHeight;
     num:= ANumFilas;
   end;

   Result:= dim / num;
end;

{ TPaintBoxHelper }

function TPaintBoxHelper.GetCoordX(XPoint: Single; ANumFilas, ANumColumnas: Integer): Integer;
begin
  Result:= TTranslatorCoord.GetCoordX(Width, XPoint, ANumColumnas, GetFrecuency(ANumFilas, ANumColumnas));
end;

function TPaintBoxHelper.GetCoordY(YPoint: Single; ANumFilas, ANumColumnas: Integer): Integer;
begin
   Result:= TTranslatorCoord.GetCoordY(Height, YPoint, ANumFilas, GetFrecuency(ANumFilas, ANumColumnas));
end;

function TPaintBoxHelper.GetFrecuency(ANumFilas, ANumColumnas: Integer): Single;
begin
  Result:= GetMaxDim(Height, Width, ANumFilas, ANumColumnas);
end;


function TPaintBoxHelper.GetXYPoint(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TPointF;
begin
  Result:= TTranslatorPoint.GetXYPoint(Height, AcX, AcY, GetFrecuency(ANumFilas, ANumColumnas));
end;

function TPaintBoxHelper.GetXYRect(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TRectF;
begin
   Result:= TTranslatorPoint.GetXYRect(Height, Width, AcX, AcY, ANumFilas, ANumColumnas, GetFrecuency(ANumFilas, ANumColumnas));
end;

{ TBitmapHelper }

function TBitmapHelper.GetFrecuency(ANumFilas, ANumColumnas: Integer): Single;
begin
  Result:= GetMaxDim(Height, Width, ANumFilas, ANumColumnas);
end;


function TBitmapHelper.GetXYPoint(AcX, AcY: Integer; ANumFilas, ANumColumnas: Integer): TPointF;
begin
  Result:= TTranslatorPoint.GetXYPoint(Height, AcX, AcY, GetFrecuency(ANumFilas, ANumColumnas));
end;

function TBitmapHelper.GetXYRect(AcX, AcY, ANumFilas,
  ANumColumnas: Integer): TRectF;
begin
   Result:= TTranslatorPoint.GetXYRect(Height, Width, AcX, AcY, ANumFilas, ANumColumnas, GetFrecuency(ANumFilas, ANumColumnas));
end;

{ TTranslatorCoord }

class function TTranslatorCoord.GetCoordX(AWidth, XPoint: Single;
  ANumColumnas: Integer; AFrecuency: Single): Integer;
var
 mx: Single;    //margen
 f: Single;
begin
   mx:= (AWidth - (ANumColumnas * AFrecuency))/2;
   f:= (XPoint -mx) / AFrecuency;
   if (f > 0) and (f < (ANumColumnas)) then
   begin
     Result:= Trunc(f);
     if Exp(f) > 0 then
       Result:= Result + 1;
   end
   else Result:= -1;
end;

class function TTranslatorCoord.GetCoordY(AHeight, YPoint: Single;
  ANumFilas: Integer; AFrecuency: Single): Integer;
var
 my: Single;             //margenes
 f: Single;
begin
   my:= (AHeight - (ANumFilas * AFrecuency))/2;
   f:= (YPoint -my) / AFrecuency;

   if (f > 0) and (f < (ANumFilas)) then
   begin
     Result:= ANumFilas - Trunc(f);
   end
   else Result:= -1;
end;

{ TTranslatorPoint }

class function TTranslatorPoint.GetXYPoint(AHeight: Single; AcX,
  AcY: Integer; AFrecuency: Single = 100.0): TPointF;
var
  X,Y: Single;
begin
  X:= ((AcX-1) * AFrecuency) + (AFrecuency/2);
  Y:= AHeight - ((AcY * AFrecuency) + (AFrecuency/2));

  Result:= TPointF.Create(X,Y);
end;


class function TTranslatorPoint.GetXYRect(AHeight, AWidth: Single; AcX, AcY,
  ANumFilas, ANumColumnas: Integer; AFrecuency: Single = 100.0): TRectF;
var
 a,b: TPointF;
 mx, my: Single;         //margenes
begin
   mx:= (AWidth - (ANumColumnas * AFrecuency))/2;
   my:= (AHeight - (ANumFilas * AFrecuency))/2;

   a:= TTranslatorPoint.GetXYPoint(AHeight, AcX-1, AcY, AFrecuency);
   a.X := a.X + (AFrecuency/2)+mx;
   a.Y := a.Y + (AFrecuency/2)-my;

   b:= TTranslatorPoint.GetXYPoint(AHeight, AcX, AcY-1, AFrecuency);
   b.X := b.X + (AFrecuency/2)+mx;
   b.Y := b.Y + (AFrecuency/2)-my;

   Result:= TRectF.Create(a, b);
end;


end.

