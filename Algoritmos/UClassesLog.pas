unit UClassesLog;

{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClassesLog

Objetivo: Sirve de reporte para comprobar que el algoritmo esta ejecutandose
          de la forma deseada. Durante la implementaci�n del algoritmo,
          se necesita evaluar los puntos no solo del movimiento hecho sino
          de la cola generada de movimientos posibles, y de la elecci�n
          heuristica efectuada para extraer el movimiento optimo. Esta elecci�n
          se hace en funcion de la funci�n que establece la prioridad, basada
          en este caso en la distancia hamilton y la distancia a la posicion
          de origen. Se intentan mostrar en el log datos relevantes para el
          analisis. Uno de esos datos relevantes es el PuzzlePrevio que representa
          la posicion previa al moviento actual.


Ejemplo de reporte:  -log.txt-


          MUEVES Prioridad:<0,6,6>=12(0) [0,3,1,6,5,4,7,8,2] <Null>

          ----- COLA ---0--------------
          ------------------------------------------

          ----- COLA ---2--------------
          ENCOLADO Prioridad:<1,7,7>=15(1) [3,0,1,6,5,4,7,8,2]
          ENCOLADO Prioridad:<1,5,5>=11(3) [6,3,1,0,5,4,7,8,2]
          ------------------------------------------
          EXTRAE Prioridad:<1,5,5>=11(3) [6,3,1,0,5,4,7,8,2] | (0) [0,3,1,6,5,4,7,8,2]
          MUEVES Prioridad:<1,5,5>=11(3) [6,3,1,0,5,4,7,8,2] | (0) [0,3,1,6,5,4,7,8,2]

          ----- COLA ---3--------------
          ENCOLADO Prioridad:<1,7,7>=15(1) [3,0,1,6,5,4,7,8,2]
          ENCOLADO Prioridad:<2,5,6>=13(4) [6,3,1,5,0,4,7,8,2]
          ENCOLADO Prioridad:<2,4,4>=10(6) [6,3,1,7,5,4,0,8,2]
          ------------------------------------------
          EXTRAE Prioridad:<2,4,4>=10(6) [6,3,1,7,5,4,0,8,2] | (3) [6,3,1,0,5,4,7,8,2]
          MUEVES Prioridad:<2,4,4>=10(6) [6,3,1,7,5,4,0,8,2] | (3) [6,3,1,0,5,4,7,8,2]

          ----- COLA ---3--------------
          ENCOLADO Prioridad:<1,7,7>=15(1) [3,0,1,6,5,4,7,8,2]
          ENCOLADO Prioridad:<2,5,6>=13(4) [6,3,1,5,0,4,7,8,2]
          ENCOLADO Prioridad:<3,3,3>=9(7) [6,3,1,7,5,4,8,0,2]
          ------------------------------------------
          EXTRAE Prioridad:<3,3,3>=9(7) [6,3,1,7,5,4,8,0,2] | (6) [6,3,1,7,5,4,0,8,2]
          MUEVES Prioridad:<3,3,3>=9(7) [6,3,1,7,5,4,8,0,2] | (6) [6,3,1,7,5,4,0,8,2]

          ----- COLA ---4--------------
          ENCOLADO Prioridad:<1,7,7>=15(1) [3,0,1,6,5,4,7,8,2]
          ENCOLADO Prioridad:<2,5,6>=13(4) [6,3,1,5,0,4,7,8,2]
          ENCOLADO Prioridad:<4,4,4>=12(8) [6,3,1,7,5,4,8,2,0]
          ENCOLADO Prioridad:<4,2,2>=8(4) [6,3,1,7,0,4,8,5,2]
          ------------------------------------------
          EXTRAE Prioridad:<4,2,2>=8(4) [6,3,1,7,0,4,8,5,2] | (7) [6,3,1,7,5,4,8,0,2]
          MUEVES Prioridad:<4,2,2>=8(4) [6,3,1,7,0,4,8,5,2] | (7) [6,3,1,7,5,4,8,0,2]

          ----- COLA ---6--------------
          ENCOLADO Prioridad:<1,7,7>=15(1) [3,0,1,6,5,4,7,8,2]
          ENCOLADO Prioridad:<2,5,6>=13(4) [6,3,1,5,0,4,7,8,2]
          ENCOLADO Prioridad:<4,4,4>=12(8) [6,3,1,7,5,4,8,2,0]
          ENCOLADO Prioridad:<5,1,1>=7(5) [6,3,1,7,4,0,8,5,2]
          ENCOLADO Prioridad:<5,3,3>=11(1) [6,0,1,7,3,4,8,5,2]
          ENCOLADO Prioridad:<5,3,3>=11(3) [6,3,1,0,7,4,8,5,2]
          ------------------------------------------
          EXTRAE Prioridad:<5,1,1>=7(5) [6,3,1,7,4,0,8,5,2] | (4) [6,3,1,7,0,4,8,5,2]
          MUEVES Prioridad:<5,1,1>=7(5) [6,3,1,7,4,0,8,5,2] | (4) [6,3,1,7,0,4,8,5,2]

          ----- COLA ---7--------------
          ENCOLADO Prioridad:<1,7,7>=15(1) [3,0,1,6,5,4,7,8,2]
          ENCOLADO Prioridad:<2,5,6>=13(4) [6,3,1,5,0,4,7,8,2]
          ENCOLADO Prioridad:<4,4,4>=12(8) [6,3,1,7,5,4,8,2,0]
          ENCOLADO Prioridad:<5,3,3>=11(1) [6,0,1,7,3,4,8,5,2]
          ENCOLADO Prioridad:<5,3,3>=11(3) [6,3,1,0,7,4,8,5,2]
          ENCOLADO Prioridad:<6,0,0>=6(2) [6,3,0,7,4,1,8,5,2]
          ENCOLADO Prioridad:<6,2,2>=10(8) [6,3,1,7,4,2,8,5,0]
          ------------------------------------------
          EXTRAE Prioridad:<6,0,0>=6(2) [6,3,0,7,4,1,8,5,2] | (5) [6,3,1,7,4,0,8,5,2]
          MUEVES Prioridad:<6,0,0>=6(2) [6,3,0,7,4,1,8,5,2] | (5) [6,3,1,7,4,0,8,5,2]

          ...


}


interface

uses System.Classes, System.SysUtils, Generics.Collections, UClasePuzzle;


type
  TLog = class(TStringList)
  public
    procedure Simulado(APuzzle: TPuzzle);
    procedure Movimiento(APuzzle: TPuzzle);
    procedure Guardar(const AFilename: String = 'log.txt');
    procedure ColaPrioridad(AMovimientos: TList<TPuzzle>);
    procedure LogText(const ALog: String);
  end;


function LogIt: TLog;


implementation


var FLog: TLog = Nil;

function LogIt: TLog;
begin
  if FLog = nil then
    FLog := TLog.Create;
  Result := FLog;
end;


{ TLog }

{$region 'Log para test movimientos '}

procedure TLog.ColaPrioridad(AMovimientos: TList<TPuzzle>);
var
  Puzzle: TPuzzle;
begin
   Add('');
   Add('----- COLA ---'+ AMovimientos.Count.ToString +'--------------');

   for Puzzle in AMovimientos do
     Add('ENCOLADO Prioridad:<' + Puzzle.Coste.ToString + ','+
                                  Puzzle.Desplazadas.ToString + ','+
                                  Puzzle.DistanciaManhatan.ToString +'>='+
                                  Puzzle.Prioridad.ToString + '('+Puzzle.GetIDXCero.ToString + ') ' + Puzzle.ToString);

   Add('------------------------------------------');
end;


procedure TLog.Guardar(const AFilename: String);
begin
   SaveToFile(AFilename);
end;

procedure TLog.LogText(const ALog: String);
begin
  Add(ALog);
end;

procedure TLog.Simulado(APuzzle: TPuzzle);
begin
     if Assigned(APuzzle.PuzzlePrevio) then
       Add('EXTRAE Prioridad:<' + APuzzle.Coste.ToString + ','+
                                  APuzzle.Desplazadas.ToString + ','+
                                  APuzzle.DistanciaManhatan.ToString +'>='+
                                  APuzzle.Prioridad.ToString + '('+APuzzle.GetIDXCero.ToString + ') ' + APuzzle.ToString + ' | ('+APuzzle.PuzzlePrevio.GetIDXCero.ToString + ') ' + APuzzle.PuzzlePrevio.ToString)
     else
       Add('EXTRAE Prioridad:<' + APuzzle.Coste.ToString + ','+
                                  APuzzle.Desplazadas.ToString + ','+
                                  APuzzle.DistanciaManhatan.ToString +'>='+
                                  APuzzle.Prioridad.ToString + '('+APuzzle.GetIDXCero.ToString + ') ' + APuzzle.ToString + ' <Null>');;
end;

procedure TLog.Movimiento(APuzzle: TPuzzle);
begin
     if Assigned(APuzzle.PuzzlePrevio) then
       Add('MUEVES Prioridad:<' + APuzzle.Coste.ToString + ','+
                                  APuzzle.Desplazadas.ToString + ','+
                                  APuzzle.DistanciaManhatan.ToString +'>='+
                                  APuzzle.Prioridad.ToString + '('+APuzzle.GetIDXCero.ToString + ') ' + APuzzle.ToString + ' | ('+APuzzle.PuzzlePrevio.GetIDXCero.ToString + ') ' + APuzzle.PuzzlePrevio.ToString)
     else
       Add('INICIO Prioridad:<' + APuzzle.Coste.ToString + ','+
                                  APuzzle.Desplazadas.ToString + ','+
                                  APuzzle.DistanciaManhatan.ToString +'>='+
                                  APuzzle.Prioridad.ToString + '('+APuzzle.GetIDXCero.ToString + ') ' + APuzzle.ToString + ' <Null>');;
end;

{$endregion}

initialization
  LogIt;

finalization
  LogIt.Free;


end.
