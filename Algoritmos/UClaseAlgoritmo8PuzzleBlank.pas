unit UClaseAlgoritmo8PuzzleBlank;


{
*******************************************************************************
Version 2017.9.14.1
*******************************************************************************
El blog de Delphi Basico
Autor: Salvador Jover
*******************************************************************************

Unidad: UClaseAlgoritmo8PuzzleBlank

Objetivo: Sobrescribe (o extiende) la clase base TAlgoritmoThread para
          implementar un metodo de resoluci�n que siempre se va a ejecutar
          en el contexto de un hilo, sincronizandose cuando es necesario
          con el hilo principal para comunicar o notificar de cambios en
          el estado del problema, como por ejemplo sucede al encontrar el
          punto de resolucion.

          Nota: En la seccion de inicializacion se registra la clase para
          que pueda ser invocada por nombre desde la ventana principal, a
          traves del metodo del puzle que resuelve

          Ej: (ver UMainGame.pas)
            Puzzle.ResolverGrafo(ModuleInfoManager.GetModuleClassByName('TAlgoritmo8puzzleBlank'));

          Esto nos permite, registrar tantos resolutores del problema
          como deseemos que intereactuarian sobre el puzzle, bien en competici�n
          bien en colaboraci�n, puesto que cada resolutor se ejecuta sobre
          un hilo separado.

}


interface


uses UClaseGrafo;

type
  TAlgoritmo8puzzleBlank = class(TAlgoritmoThread)
  private
  protected
    procedure Run; override;
  public
  end;


implementation

uses System.Classes, System.SysUtils, Generics.Collections, UClaseRegistro,
UClasePuzzle;

procedure TAlgoritmo8puzzleBlank.Run;
begin
    { TODO : A�adir implementacion algoritmo... }
end;


initialization
  ModuleInfoManager.RegisterModule('TAlgoritmo8puzzleBlank', TAlgoritmo8puzzleBlank, 'Algoritmo de 8-Puzzle Vac�o');

end.
