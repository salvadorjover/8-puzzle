﻿# README #

Resolucion del puzzle-8 para el blog de Delphi Basico
Autor: Salvador Jover
Fecha: Septiempbre de 2017

  El blog de Delphi Básico no tiene ánimo de lucro y su fin es el de colaborar
  a la divulgación de conocimientos dentro de la comunidad de programadores de
  Delphi, desde su nacimiento en el año 2003.

  ******************************************************************************
  ******************************************************************************
  ******************************************************************************
  EL SOFTWARE SE PROPORCIONA "TAL CUAL", SIN GARANTÍA DE NINGÚN TIPO, EXPRESA O 
  IMPLÍCITA, INCLUYENDO PERO NO LIMITADO A GARANTÍAS DE COMERCIALIZACIÓN, IDONEIDAD 
  PARA UN PROPÓSITO PARTICULAR Y NO INFRACCIÓN. EN NINGÚN CASO LOS AUTORES O 
  TITULARES DEL COPYRIGHT SERÁN RESPONSABLES DE NINGUNA RECLAMACIÓN, DAÑOS U 
  OTRAS RESPONSABILIDADES, YA SEA EN UNA ACCIÓN DE CONTRATO, AGRAVIO O CUALQUIER 
  OTRO MOTIVO, QUE SURJA DE O EN CONEXIÓN CON EL SOFTWARE O EL USO U OTRO TIPO 
  DE ACCIONES EN EL SOFTWARE.
  ******************************************************************************
  ******************************************************************************
  ******************************************************************************

  Este código complementa la entrada del blog de Delphi Básico:
  
  Un artilugio para experimentar... o algo así.
  https://delphibasico.wordpress.com/?p=10523&preview=true
  
  Para la discusión del contenido, se ha creado un grupo en la Community
  
  https://community.embarcadero.com/my-groups/viewgroup/580-taller-de-delphi-basico
  
  donde es necesario registrarse para acceder. Dicho grupo es creado tambien sin 
  ánimo de lucro, para la discusión en modo taller del contenido de alguna de las
  entradas del blog.
  
 Ver video: 
 https://drive.google.com/file/d/0B74jWs5joTMcakpvX0VCeVRMRDA/view?usp=sharing 


  Funcionalidades que permite el código:
  
          -Permite mover una casilla o varias (clic en el tablero).
          -Genera movimientos aleatorios para la posición inicial
          -Inicializar la posición inicial.
          -Ejecutar la resolución del problema. (**)
		  -Visualizar los pasos que ha seleccionado el algoritmo para la resolucion.

          * No advierte de problema no resoluble puesto que por
          definición siempre tendría solución, dado que los moviemientos
          aleatorios se generan siempre de forma lícita y no es posible
          alterar el tablero a una posición no consistente o irresoluble.

         ** Propuesto algoritmo A* (AStar) para ver de forma separada en un taller del
            blog, abierto al efecto de ampliar y compartir ideas sobre el contenido
            de las entradas del blog y especificamente de esta serie.

  El código ha sido compilado con RAD Studio 10.2 Tokyo Update 1, con la
  libreria FMX, para Windows. Es posible cualquier adaptación a otra plataforma
  de escritorio o movil de la parte de usabilidad del juego.

  Se ha incluido en el repositorio una imagen jpg 'test.jpg', que el proyecto
  intentará localizar en la ruta del ejecutable. Copiar la imagen o cualquier
  otra de dimensiones  similares en esta localización, para visualizar el ajuste.
  Dado que no es una version comercial, se ha omitido cualquier rutina de adaptación
  de tamaño, proporción o búsqueda en directorios de la plataforma en la que se
  pueda compilar. La imagen se incluye solo a efectos de ilustrar esta posibilidad.
  
  Bibliografia: Inteligencia Artificial de Virginie Mathivet. (*)
  
  (*) Se han tomado algunas ideas del codigo que propone en su libro la autora para el
  capítulo del libro sobre Busqueda de Rutas. Ver la entrada del blog:  
  https://delphibasico.com/2017/08/27/tiempo-para-leer-virginie-mathivet/
  
  
  
